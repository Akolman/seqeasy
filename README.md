# Seqeasy

[![pipeline status](https://gitlab.com/Akolman/Seqeasy/badges/master/pipeline.svg)](https://gitlab.com/Akolman/Seqeasy/-/commits/master)

Lite-weight and easy-to-use sequence generating library using buildable patterns.  Provides built-in support for a number of standard types, as well as support for custom types.  Sequences can be built using pre-defined or custom character sets.  Also provides a thread-safe context.  Supports .netstandard2.1 and on.

### Pattern Types
- **Constant**: Prints a constant value to a sequence
- **Random character/letter/number**: Prints a random letter, number, or character (letter or number) to the sequence.
- **Incrementer**: Prints an incrementing value to the sequence.  Portions of incrementing value can be split within sequence using "increment chain".
- **DateTime**: Prints a date/time value to the sequence.

### How to use
The simplest way to get started is by using the `PatternBuilder` class using a default character set (character sets can be found within the `Seqeasy.CharacterSets` namespace.

```
using Seqeasy;
using Seqeasy.CharacterSets.Special;

var builder = new PatternBuilder<HexCharSet>();
```

`HexCharSet` is a `CharacterSet` that uses the hexadecimal characters `[0-9A-F]`.

`PatternBuilder` methods can be chained.  Upon finalizing the building, `GetGenerator` can be called to produce a `SequenceGenerator`:

```
var generator = builder.AddConst("H").AddChar(4).GetGenerator();
```

In this example, a sequence value is produced by calling `generator.GetNext()`.  The following code:

```
for (var i=0; i< 5; i++)
{
    Console.WriteLine(generator.GetNext());
}
```

will produce output similar to:

```
H97E2
HF1CD
H273A
H62A2
H799A
```

To use an incrementer, simply call `PatterhBuilder.AddIncrement`:

```
var generator = builder.AddConst("H").AddChar(1).AddIncrement(4).GetGenerator();
```

This will produce the following:

```
H50000
H70001
H20002
H50003
H10004
H70005
H10006
HC0007
H30008
H10009
H5000A
H1000B
HF000C
H6000D
H2000E
HE000F
H40010
H50011
HF0012
HA0013
``` 

## Extensions

- `Seqeasy.Extensions`: Set of experimental token/section types that are not in main `Seqeasy` library.
- `Seqeasy.StringPattern`: Experimental class used to consume and produce a string pattern to generate generators.
- `Seqeasy.StringPattern.Electrician`: Source generator for `Seqeasy.StringPattern` used for type discovery to locate token types.
- `Seqeasy.Tests`: Test set for `Seqeasy` core.

Preview at [akolman.info](https://guid.akolman.info/sg)