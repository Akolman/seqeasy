﻿using System;
using System.Text;

using Seqeasy.Design;

namespace Seqeasy.StringPattern.Converter
{
    internal class RandomSymbolTokenConverter : ITokenConverter<RandomSymbolSectionToken>
    {
        private readonly string? charset;

        /// <summary>
        /// Initializes a new instance of the <see cref="RandomNumberTokenConverter"/> class.
        /// </summary>
        public RandomSymbolTokenConverter()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RandomSymbolTokenConverter"/> class.
        /// </summary>
        /// <param name="characterSet">The default character set.</param>
        public RandomSymbolTokenConverter(CharacterSets.CharacterSet characterSet)
        {
            charset = characterSet.Symbols;
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        public char[] TokenIdentifiers => new char[] { 'S' };

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        public string GetStringPattern(RandomSymbolSectionToken token)
        {
            var sb = new StringBuilder();
            sb.Append('S');

            if (token.Width > 1)
            {
                sb.Append(token.Width);
            }

            return sb.ToString();
        }

        public int Parse(char[] chars, int index, out RandomSymbolSectionToken token)
        {
            (index, var width) = ITokenConverter.ParseWidth(chars, index);

            (index, _) = ITokenConverter.ParseOptions(chars, index);

            if (charset == null)
            {
                throw new Exception("charset is null");
            }

            token = new RandomSymbolSectionToken(charset, width);

            return index;
        }
    }
}
