﻿using System;
using System.Collections.Generic;
using System.Text;

using Seqeasy.Design;

namespace Seqeasy.StringPattern.Converter
{
    internal class IncrementTokenConverter : ITokenConverter<IncrementSectionToken>
    {
        private readonly string? charset;

        internal IncrementTokenConverter()
        {
        }

        internal IncrementTokenConverter(CharacterSets.CharacterSet characterSet)
        {
            charset = characterSet.Chars;
        }

        public char[] TokenIdentifiers => new char[] { 'I' };

        public string GetStringPattern(IncrementSectionToken token)
        {
            var sb = new StringBuilder();
            sb.Append('I');

            if (token.Width > 1)
            {
                sb.Append(token.Width);
            }

            if (token.IncrementAmount != 1 || token.StartValue != 0 || token.ChainId != 1)
            {
                var dic = new Dictionary<string, string>();

                if (token.IncrementAmount != 1)
                {
                    if (token.IncrementAmount > 0)
                    {
                        dic.Add($"+{token.IncrementAmount}", string.Empty);
                    }
                    else
                    {
                        dic.Add(token.IncrementAmount.ToString(), string.Empty);
                    }
                }

                if (token.StartValue != 0)
                {
                    dic.Add("-s", token.StartValue.ToString());
                }

                if (token.ChainId != 1)
                {
                    dic.Add("-c", token.ChainId.ToString());
                }

                sb.Append(ITokenConverter.StringifyArgs(dic));
            }

            return sb.ToString();
        }

        public int Parse(char[] chars, int index, out IncrementSectionToken token)
        {
            var incVal = 1;
            var startVal = 0;

            (index, var width) = ITokenConverter.ParseWidth(chars, index);

            (index, var opts) = ITokenConverter.ParseOptions(chars, index);

            foreach (var opt in opts)
            {
                switch (opt.Key)
                {
                    case "s":
                        startVal = int.Parse(opt.Value);
                        break;
                    case "S":
                        startVal = int.Parse(opt.Value);
                        break;
                    case "i":
                        incVal = int.Parse(opt.Value);
                        break;
                    case "I":
                        incVal = int.Parse(opt.Value);
                        break;
                }
            }

            token = new IncrementSectionToken(charset!, width, incVal, startVal);

            return index;
        }
    }
}
