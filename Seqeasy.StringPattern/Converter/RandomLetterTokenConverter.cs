﻿using System;
using System.Text;

using Seqeasy.Design;

namespace Seqeasy.StringPattern.Converter
{
    internal class RandomLetterTokenConverter : ITokenConverter<RandomLetterSectionToken>
    {
        private readonly string? charset;

        internal RandomLetterTokenConverter()
        {
        }

        internal RandomLetterTokenConverter(CharacterSets.CharacterSet characterSet)
        {
            charset = characterSet.Letters;
        }

        public char[] TokenIdentifiers => new char[] { 'L', 'l' };

        public string GetStringPattern(RandomLetterSectionToken token)
        {
            var sb = new StringBuilder();
            sb.Append('L');

            if (token.Width > 1)
            {
                sb.Append(token.Width);
            }

            if (token.CaseSpecifier != CaseSpecifier.Unspecified)
            {
                sb.Append($@"(-{(int)token.CaseSpecifier})");
            }

            return sb.ToString();
        }

        public int Parse(char[] chars, int index, out RandomLetterSectionToken token)
        {
            var caseSpec = CaseSpecifier.Unspecified;

            if (chars[index] == 'l')
            {
                caseSpec = CaseSpecifier.LowercaseOnly;
            }

            (index, var width) = ITokenConverter.ParseWidth(chars, index);

            (index, var opts) = ITokenConverter.ParseOptions(chars, index);

            foreach (var opt in opts)
            {
                switch (opt.Key)
                {
                    case "1":
                        caseSpec = CaseSpecifier.UppercaseOnly;
                        break;
                    case "2":
                        caseSpec = CaseSpecifier.LowercaseOnly;
                        break;
                    case "0":
                        caseSpec = CaseSpecifier.Unspecified;
                        break;
                }
            }

            if (charset == null)
            {
                throw new Exception("charset is null");
            }

            token = new RandomLetterSectionToken(charset, width, caseSpec);

            return index;
        }
    }
}
