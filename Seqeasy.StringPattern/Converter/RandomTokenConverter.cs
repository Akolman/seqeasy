﻿using System;
using System.Text;

using Seqeasy.Design;

namespace Seqeasy.StringPattern.Converter
{
    internal class RandomTokenConverter : ITokenConverter<RandomSectionToken>
    {
        private readonly string? charset;

        internal RandomTokenConverter()
        {
        }

        internal RandomTokenConverter(CharacterSets.CharacterSet characterSet)
        {
            charset = characterSet.Chars;
        }

        public char[] TokenIdentifiers => new char[] { 'R', 'r' };

        public string GetStringPattern(RandomSectionToken token)
        {
            var sb = new StringBuilder();
            sb.Append('R');

            if (token.Width > 1)
            {
                sb.Append(token.Width);
            }

            if (token.CaseSpecifier != CaseSpecifier.Unspecified)
            {
                sb.Append($@"(-{(int)token.CaseSpecifier})");
            }

            return sb.ToString();
        }

        public int Parse(char[] chars, int index, out RandomSectionToken token)
        {
            var caseSpec = CaseSpecifier.Unspecified;

            if (chars[index] == 'r')
            {
                caseSpec = CaseSpecifier.LowercaseOnly;
            }

            (index, var width) = ITokenConverter.ParseWidth(chars, index);

            (index, var opts) = ITokenConverter.ParseOptions(chars, index);

            foreach (var opt in opts)
            {
                switch (opt.Key)
                {
                    case "1":
                        caseSpec = CaseSpecifier.UppercaseOnly;
                        break;
                    case "2":
                        caseSpec = CaseSpecifier.LowercaseOnly;
                        break;
                    case "0":
                        caseSpec = CaseSpecifier.Unspecified;
                        break;
                }
            }

            if (charset == null)
            {
                throw new Exception("charset is null");
            }

            token = new RandomSectionToken(charset, width, caseSpec);

            return index;
        }
    }
}
