﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Seqeasy.Design;

namespace Seqeasy.StringPattern.Converter
{
    /// <summary>
    /// Represents an interface used to define a converter which converts a string pattern
    /// to and from a token.
    /// </summary>
    public interface ITokenConverter
    {
        /// <summary>
        /// Converts a Dictionary of arguments to a string-d argument list.
        /// </summary>
        /// <param name="args">The argument dictionary.</param>
        /// <returns>A string pattern parameter list.</returns>
        protected static string StringifyArgs(Dictionary<string, string> args)
        {
            if (args.Count == 0)
            {
                return string.Empty;
            }

            var paramStringBuilder = new StringBuilder(32);

            paramStringBuilder.Append('(');

            foreach (var arg in args)
            {
                paramStringBuilder.Append(arg.Key);

                if (!string.IsNullOrEmpty(arg.Value))
                {
                    paramStringBuilder.Append(' ');
                    paramStringBuilder.Append(arg.Value);
                }

                if (!args.Last().Equals(arg))
                {
                    paramStringBuilder.Append(' ');
                }
            }

            paramStringBuilder.Append(')');

            return paramStringBuilder.ToString();
        }

        /// <summary>
        /// Will parse a character array for digits following the current character at 'index'
        /// until no more numbers are reached.  index will be returned as the number of the character
        /// at the last width character value (or the identifier value).  width will hold the width, or 1 if no width parsed.
        /// </summary>
        /// <param name="chars">The character array.</param>
        /// <param name="index">The current position in the character array.</param>
        /// <returns>The index of the last character, and the width determined.</returns>
        protected static (int Index, int Width) ParseWidth(char[] chars, int index)
        {
            var width = 1;
            if (chars.Length > index + 1 && char.IsDigit(chars[index + 1]))
            {
                var widthVal = string.Empty;
                do
                {
                    widthVal += chars[index++ + 1];
                }
                while (chars.Length > index + 1 && char.IsDigit(chars[index + 1]));

                width = int.Parse(widthVal);
            }

            return (index, width);
        }

        /// <summary>
        /// Parses a character array for options, and returns the options and index of the last character.
        /// </summary>
        /// <param name="chars">The character array.</param>
        /// <param name="index">The index to begin checking for options.</param>
        /// <returns>The index of the last character, and the options dictionary.</returns>
        protected static (int Index, Dictionary<string, string> Options) ParseOptions(char[] chars, int index)
        {
            Dictionary<string, string> opts = new();

            if (index + 1 >= chars.Length || chars[index + 1] != '(')
            {
                return (index, opts);
            }

            index++;

            var optString = string.Empty;

            try
            {
                while (chars[++index] != ')')
                {
                    optString += chars[index];
                }
            }
            catch (Exception e)
            {
                throw new Exceptions.BadOptionStringException(new string(chars), "No closing parenthesis found", e);
            }

#if NET5_0_OR_GREATER
            var optArray = optString.Split('-', StringSplitOptions.RemoveEmptyEntries | StringSplitOptions.TrimEntries);
#else
            var optArray = optString.Split('-');
            optArray.ToList().ForEach((s) => s = s.Trim());
#endif
            foreach (var opt in optArray)
            {
                if (opt.Length == 0)
                {
                    continue;
                }
                else if (opt.Length == 1)
                {
                    opts.Add(opt, string.Empty);
                }
                else
                {
                    if (opt.IndexOf(' ') > -1)
                    {
                        var parts = opt.Split(' ');
                        opts.Add(parts[0], parts[1]);
                    }
                    else
                    {
                        var keyPart = string.Empty;
                        var valuePart = string.Empty;
                        var keyEnded = false;
                        foreach (var ch in opt)
                        {
                            if (char.IsLetter(ch) && !keyEnded)
                            {
                                keyPart += ch;
                            }
                            else if (keyEnded || char.IsDigit(ch))
                            {
                                valuePart += ch;
                            }
                        }

                        opts.Add(keyPart, valuePart);
                    }
                }
            }

            return (index, opts);
        }
    }

    /// <summary>
    /// Represents a class used to define to/from converters for a <see cref="SectionToken"/>.
    /// </summary>
    /// <typeparam name="T">The SectionToken type defined by this ITokenConverter.</typeparam>
    public interface ITokenConverter<T> : ITokenConverter
        where T : SectionToken
    {
        /// <summary>
        /// Gets the identifying character for this <see cref="ITokenConverter{T}"/>.
        /// </summary>
        public char[] TokenIdentifiers { get; }

        /// <summary>
        /// Parses a char array for a token starting a index and returns the token.
        /// </summary>
        /// <param name="chars">The input char array.</param>
        /// <param name="index">The current index in chars where this token begins.</param>
        /// <param name="token">Upon success where to place the token.</param>
        /// <returns>Where to advance the index after parsing this token (place at start of next token).</returns>
        public int Parse(char[] chars, int index, out T token);

        /// <summary>
        /// Produces a string pattern for token T.
        /// </summary>
        /// <param name="token">The token in which to generate a pattern for.</param>
        /// <returns>The string pattern for this token.</returns>
        public string GetStringPattern(T token);
    }
}
