﻿using System;

using Seqeasy.Design;

namespace Seqeasy.StringPattern.Converter
{
    internal class DateTimeTokenConverter : ITokenConverter<DateTimeSectionToken>
    {
        private readonly DateTimeSectionToken defaultTokenItem;

        internal DateTimeTokenConverter()
        {
            defaultTokenItem = new DateTimeSectionToken();
        }

        public char[] TokenIdentifiers => new char[] { 'D' };

        public string GetStringPattern(DateTimeSectionToken token)
        {
            if (token.Format == defaultTokenItem.Format)
            {
                return "D";
            }

            return $"D(-f {token.Format})";
        }

        public int Parse(char[] chars, int index, out DateTimeSectionToken token)
        {
            var format = defaultTokenItem.Format;

            (index, var opts) = ITokenConverter.ParseOptions(chars, index);

            foreach (var opt in opts)
            {
                switch (opt.Key)
                {
                    case "f":
                        format = opt.Value;
                        break;
                }
            }

            token = new DateTimeSectionToken(format);

            return index;
        }
    }
}
