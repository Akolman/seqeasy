﻿using System;

using Seqeasy.Design;

namespace Seqeasy.StringPattern.Converter
{
    internal class ConstTokenConverter : ITokenConverter<ConstSectionToken>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ConstTokenConverter"/> class.
        /// </summary>
        internal ConstTokenConverter()
        {
        }

        public char[] TokenIdentifiers => new char[] { '[' };

        public string GetStringPattern(ConstSectionToken token)
        {
            return $"[{token.ConstValue}]";
        }

        public int Parse(char[] chars, int index, out ConstSectionToken token)
        {
            var constValue = string.Empty;

            for (; index < chars.Length; index++)
            {
                var curChar = chars[index];
                if (curChar == '[')
                {
                    continue;
                }
                else if (curChar == ']')
                {
                    break;
                }

                constValue += curChar;
            }

            token = new ConstSectionToken(constValue);

            return index;
        }
    }
}
