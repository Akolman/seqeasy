﻿using System;
using System.Text;

using Seqeasy.Design;

namespace Seqeasy.StringPattern.Converter
{
    internal class RandomNumberTokenConverter : ITokenConverter<RandomNumberSectionToken>
    {
        private readonly string? charset;

        /// <summary>
        /// Initializes a new instance of the <see cref="RandomNumberTokenConverter"/> class.
        /// </summary>
        public RandomNumberTokenConverter()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RandomNumberTokenConverter"/> class.
        /// </summary>
        /// <param name="characterSet">The default character set.</param>
        public RandomNumberTokenConverter(CharacterSets.CharacterSet characterSet)
        {
            charset = characterSet.Numbers;
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        public char[] TokenIdentifiers => new char[] { 'N' };

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        public string GetStringPattern(RandomNumberSectionToken token)
        {
            var sb = new StringBuilder();
            sb.Append('N');

            if (token.Width > 1)
            {
                sb.Append(token.Width);
            }

            return sb.ToString();
        }

        public int Parse(char[] chars, int index, out RandomNumberSectionToken token)
        {
            (index, var width) = ITokenConverter.ParseWidth(chars, index);

            (index, _) = ITokenConverter.ParseOptions(chars, index);

            if (charset == null)
            {
                throw new Exception("charset is null");
            }

            token = new RandomNumberSectionToken(charset, width);

            return index;
        }
    }
}
