﻿using System;

using Seqeasy.CharacterSets;

namespace Seqeasy.StringPattern
{
    /// <summary>
    /// Extension methods for <see cref="Seqeasy"/> objects.
    /// </summary>
    public static class StringPatternExtensions
    {
        /// <summary>
        /// Gets a <see cref="StringPattern"/> using a <see cref="Pattern"/>.
        /// </summary>
        /// <param name="pattern">The Pattern.</param>
        /// <returns>A string representing the Pattern.</returns>
        public static string GetStringPattern(this Pattern pattern)
        {
            return Parser.ProduceString(pattern.CopyTokens());
        }

        /// <summary>
        /// Gets a <see cref="StringPattern"/> using a <see cref="PatternBuilder"/>.
        /// </summary>
        /// <param name="builder">The PatternBuilder.</param>
        /// <returns>A string representing the Pattern currently produced by the PatternBuilder.</returns>
        public static string GetStringPattern(this PatternBuilder builder)
        {
            return GetStringPattern(builder.GetPattern());
        }

        /// <summary>
        /// Gets a <see cref="StringPattern"/> using a <see cref="PatternBuilder"/>.
        /// </summary>
        /// <typeparam name="T">The default CharacterSet Type.</typeparam>
        /// <param name="builder">The PatternBuilder.</param>
        /// <returns>A string representing the Pattern currently produced by the PatternBuilder.</returns>
        public static string GetStringPattern<T>(this PatternBuilder<T> builder)
            where T : CharacterSet, new()
        {
            return GetStringPattern(builder.GetPattern());
        }
    }
}
