﻿using System;

using Seqeasy.Design;

namespace Seqeasy.StringPattern
{
    /// <summary>
    /// Interface used to define the internal wiring for parsable types.
    /// </summary>
    internal interface IConverterWiring
    {
        /// <summary>
        /// Gets an array of characters that identify this token in a string pattern.
        /// </summary>
        public char[] Identifiers { get; }

        /// <summary>
        /// Gets the last <see cref="Exception"/> set by this wiring.
        /// </summary>
        public Exception? LastException { get; }

        /// <summary>
        /// Gets the token type associated with this converter.
        /// </summary>
        public Type TokenType { get; }

        /// <summary>
        /// Parses a section of a string pattern and produces a token.
        /// </summary>
        /// <param name="chars">The characters of the string pattern.</param>
        /// <param name="index">The index where this pattern begins.</param>
        /// <returns>The index of the last char in this pattern, and the token produced.</returns>
        public (int Index, SectionToken? Token) Parse(char[] chars, int index);

        /// <summary>
        /// Produces this section's string pattern.
        /// </summary>
        /// <param name="token">The token to produce a pattern for.</param>
        /// <returns>The string pattern for this token.</returns>
        public string GetStringPattern(SectionToken token);
    }
}
