﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Seqeasy.Design;
using static Seqeasy.StringPattern.Exceptions;

namespace Seqeasy.StringPattern
{
    internal class Parser
    {
        internal static string ProduceString(IEnumerable<SectionToken> tokens)
        {
            var converters = Parser.GetConverters(Parser.GetImplementers());

            StringBuilder sb = new();
            foreach (var token in tokens)
            {
                var converter = converters.FirstOrDefault(c => c.TokenType.Equals(token.GetType()));
                sb.Append(converter.GetStringPattern(token));
            }

            return sb.ToString();
        }

        internal static IEnumerable<SectionToken> ParseTokens<T>(string pattern)
            where T : CharacterSets.CharacterSet, new()
        {
            var converters = Parser.GetConverters<T>(Parser.GetImplementers());

            var sections = new List<SectionToken>();


            var chars = pattern.ToCharArray();
            for (var i = 0; i < chars.Length; i++)
            {
                var curChar = chars[i];
                var converter = converters.FirstOrDefault(c => c.Identifiers.Contains(curChar));
                if (converter == null)
                {
                    throw new UnknownIdentifierException(curChar);
                }

                (i, var token) = converter.Parse(chars, i);
                if (token == null)
                {
                    if (converter.LastException != null)
                    {
                        throw new MalformedStringPatternException(pattern, converter.LastException);
                    }
                    else
                    {
                        throw new MalformedStringPatternException(pattern);
                    }
                }

                sections.Add(token);
            }

            return sections;
        }

        /// <summary>
        /// Gets all discovered string pattern implmeneters.
        /// </summary>
        /// <returns>An array of implementers.</returns>
        internal static IEnumerable<Type> GetImplementers()
        {
            return Wirings.DiscoveredConverters;
        }

        internal static List<IConverterWiring> GetConverters(IEnumerable<Type> implementers)
        {
            var converters = new List<IConverterWiring>(implementers.Count());

            foreach (var converterType in implementers)
            {
                if (Activator.CreateInstance(converterType) is IConverterWiring converter)
                {
                    converters.Add(converter);
                }
            }

            return converters;
        }

        internal static List<IConverterWiring> GetConverters<T>(IEnumerable<Type> implementers)
            where T : CharacterSets.CharacterSet, new()
        {
            var converters = new List<IConverterWiring>(implementers.Count());

            foreach (var converterType in implementers)
            {
                if (Activator.CreateInstance(converterType, new T()) is IConverterWiring converter)
                {
                    converters.Add(converter);
                }
            }

            return converters;
        }
    }
}
