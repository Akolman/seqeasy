﻿using System;

namespace Seqeasy.StringPattern
{
    /// <summary>
    /// Defines a set of Seqeasy Exception types.
    /// </summary>
    public class Exceptions
    {
        /// <summary>
        /// Encountered when a token is encountered which has no converter defined.
        /// </summary>
        public sealed class UnknownIdentifierException : Exception
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="UnknownIdentifierException"/> class.
            /// </summary>
            /// <param name="identifier">The identifier that is unknown.</param>
            internal UnknownIdentifierException(char identifier)
                : base($"No converter found for identifier {identifier}")
            {
            }
        }

        /// <summary>
        /// Encountered when an option string is malformed.
        /// </summary>
        public sealed class BadOptionStringException : Exception
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="BadOptionStringException"/> class.
            /// </summary>
            /// <param name="inputString">The input string.</param>
            /// <param name="message">The error message.</param>
            internal BadOptionStringException(string inputString, string message)
                : base($"Bad input string ({inputString}): {message}")
            {
            }

            /// <summary>
            /// Initializes a new instance of the <see cref="BadOptionStringException"/> class.
            /// </summary>
            /// <param name="inputString">The input string.</param>
            /// <param name="message">The error message.</param>
            /// <param name="innerException">The exception that was encountered.</param>
            internal BadOptionStringException(string inputString, string message, Exception innerException)
                : base($"Bad input string ('{inputString}'): {message}", innerException)
            {
            }
        }

        /// <summary>
        /// Encountered when a <see cref="StringPattern"/> cannot be parsed.
        /// </summary>
        public sealed class MalformedStringPatternException : Exception
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="MalformedStringPatternException"/> class.
            /// </summary>
            /// <param name="pattern">The pattern.</param>
            internal MalformedStringPatternException(string pattern)
                : base($"Unknown error parsing malformed pattern '{pattern}'")
            {
            }

            /// <summary>
            /// Initializes a new instance of the <see cref="MalformedStringPatternException"/> class.
            /// </summary>
            /// <param name="pattern">The pattern.</param>
            /// <param name="exception">The exception thrown.</param>
            internal MalformedStringPatternException(string pattern, Exception exception)
                : base($"Malformed pattern '{pattern}' threw error: {exception.Message}", exception)
            {
            }
        }
    }
}
