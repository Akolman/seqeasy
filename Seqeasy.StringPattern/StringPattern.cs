﻿using System;

namespace Seqeasy.StringPattern
{
    /// <summary>
    /// Extension class used to convert to and from a string-representation of a pattern.
    /// </summary>
    public class StringPattern : Pattern
    {
        private StringPattern(string originalPattern, Pattern newPattern)
             : base(newPattern.Tokens, newPattern.Width, newPattern.Combinations)
        {
            OriginalPattern = originalPattern;
        }

        /// <summary>
        /// Gets the original pattern used to produce this StringPatttern.
        /// </summary>
        public string OriginalPattern { get; private set; }

        /// <summary>
        /// Parses a string into a new StringPattern.
        /// </summary>
        /// <typeparam name="T">The default CharacterSet tot use.</typeparam>
        /// <param name="pattern">The string pattern.</param>
        /// <returns>A new StringPattern.</returns>
        public static StringPattern Parse<T>(string pattern)
            where T : CharacterSets.CharacterSet, new()
        {
            var sections = Parser.ParseTokens<T>(pattern);

            return new StringPattern(pattern, new Pattern(sections));
        }
    }
}
