
# Seqeasy.StringPattern

String<->Pattern utility for [Seqeasy](https://gitlab.com/Akolman/seqeasy).

### Pattern Definitions
*Subject to change until version 1.0*

For token types that may be a variable number of characters in width, the token identifier may be followed by an integer representing the length, in chars, of that section (e.g. `R4` is the same as `RRRR`, or 4 random characters).  Where applicable, identifiers may also be followed by a set of options, enclosed by parentheses (`()`, e.g. `R2(-1)` to specify 2 uppercase random characters).


- Const (`ConstSectionToken`)
   - Const value enclosed with square brackets (`[]`).
   - `"[PREF]"`


- Date/Time (`DateTimeSectionToken`)
   - Identified by `D` character
   - `f` switch allows `DateTime` format string to be specified.  Use [DateTime](https://docs.microsoft.com/en-us/dotnet/standard/base-types/standard-date-and-time-format-strings) format specifier.  If not provided, format string `yyMMddHHmmssffff` is used.
   -  ```D(-f yyMMddHH)```


- Increment (`IncrementSectionToken`)
   - Identified by `I` character (optional width, default 1)
   - `s`/`S` to specify start value
   - `i`/`I` to specify increment value
   - (Chain specifier not yet supported)
   - ```I4(-i 5)```


- Random Letter (`RandomLetterSectionToken`)
   - Identified by `L` or `l` (lowercase `l` will specify lowercase letter generation)(optional width, default 1)
   - Switches: -0 case unspecified, -1 uppercase only, -2 lowercase only
   - ``` L4(-1)```



- Random Number (`RandomNumberSectionToken`)
   - Identified by `N`
   - ```N5```


- Random Character (`RandomSectionToken`)
   - Identified by `R` or `r` (lowercase `r` will convert any generated letter to lowercase)(optional width, default 1)
   - Switches: -0 case unspecified, -1 uppercase only, -2 lowercase only
   - :``` R4(-2)```


### Example
```
using Seqeasy;
using Seqeasy.StringPattern;
using Seqeasy.CharacterSets.CaseSensitive;

var builder = StringPattern.Parse<AlphaNumNoAmbigLetters>("[C]R(-1)NNIIII");
var gen = SequenceGenerator.Build(builder);

foreach(var seq in gen.GetEnumerator(16))
{
    Console.WriteLine(seq);
}
```
The above will produce output similar to the following:
```
CS150000
CJ510001
CR810002
CD660003
CP420004
C9070005
CH270006
CA730007
CY310008
CF900009
CM19000a
C082000b
CW02000c
CD99000d
C305000e
CQ95000f
```

The `StringPattern` namespace also adds a `Builder.GetStringPattern` extension that can be used to create a `StringPattern` from a `PatternBuilder`.  For example:

```
var builder = new PatternBuilder<AlphaNumFull>();
var stringPattern = builder.AddConst('C')
    .AddChar(caseSpecifier: CaseSpecifier.UppercaseOnly)
    .AddNumber()
    .AddNumber()
    .AddIncrement()
    .AddIncrement()
    .AddIncrement()
    .AddIncrement()
    .GetStringPattern();
```
In this scenario, `stringPattern` will result in a value of `"[C]R(-1)NNIIII"`.    

Try at [akolman.info](https://guid.akolman.info/sg)
