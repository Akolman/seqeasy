﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Seqeasy.Design;
using Seqeasy.CharacterSets.Special;
using System.Numerics;

namespace Seqeasy.Tests
{
    [TestClass]
    public class PatternTests
    {
        [TestMethod]
        public void Check_Const_Combos_Length()
        {
            var builder = new PatternBuilder<HexCharSet>();
            var g = builder.AddConst("CON");
            (var width, var combos) = g.GetDetails();

            Assert.AreEqual(3, width);
            Assert.AreEqual(1, combos);

            g = builder.AddChar(2);
            (width, combos) = g.GetDetails();

            Assert.AreEqual(5, width);
            Assert.AreEqual(256, combos);
        }

        [TestMethod]
        public void Check_ToString()
        {
            var builder = new PatternBuilder<HexCharSet>();
            var pattern = builder.AddConst('C').GetPattern();

            var patternString = pattern.ToString().Replace("\r", "").Replace("\n", "");

            Assert.AreEqual("Token1: ConstSectionToken { Width = 1, ConstValue = C, TotalCombinations = 1 }", patternString);
        }

        [TestMethod]
        public void Check_Builder_Nocalccombos()
        {
            var builder = new PatternBuilder(PatternBuilderOptions.SkipComboCalculation);
            var pattern = builder.AddChar("ABC", 500).GetPattern();

            Assert.AreEqual(-1, pattern.Combinations);
        }

        [TestMethod]
        public void Check_Charset_Builder_Nocalccombos()
        {
            var builder = new PatternBuilder<HexCharSet>(PatternBuilderOptions.SkipComboCalculation);
            var pattern = builder.AddChar(500).GetPattern();

            Assert.AreEqual(-1, pattern.Combinations);
        }
    }
}