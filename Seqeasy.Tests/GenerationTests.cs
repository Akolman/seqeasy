﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Seqeasy;
using System.Linq;
using Seqeasy.CharacterSets.Special;

namespace Seqeasy.Tests
{
    [TestClass]
    public class GenerationTests
    {
        [TestMethod]
        public void Test_Hex_Generation()
        {
            var builder = new PatternBuilder<HexCharSet>();
            var generator = builder.AddIncrement(4).GetGenerator();

            for (var i = 0; i < 65535; i++)
            {
                _ = generator.GetNext();
            }

            var last = generator.GetNext();
            Assert.AreEqual("FFFF", last);
        }

        [TestMethod]
        public void Test_Batch_Hex_Generation()
        {
            var builder = new PatternBuilder<HexCharSet>();
            var generator = builder.AddIncrement(4).GetGenerator();

            Assert.AreEqual("0000", generator.GetBatch(1).Last());
            Assert.AreEqual("0001", generator.GetNext());
            Assert.AreEqual("01F5", generator.GetBatch(500).Last());
        }

        [TestMethod]
        public void Test_Enumeration()
        {
            var builder = new PatternBuilder<HexCharSet>();
            builder.AddIncrement(5);
            var count = (int)builder.GetPattern().Combinations;

            var gen1 = builder.GetGenerator();
            var gen2 = builder.GetGenerator();

            var batch = gen1.GetBatch(count).ToList();

            var index = 0;
            foreach(var seq in gen2.GetEnumerator(count))
            {
                Assert.AreEqual(batch[index++], seq, $"Sequences at {index+1} don't match up");
            }

            Assert.AreEqual(1048576, index, "Unexpected batch size produced by enumeration");
        }

        [TestMethod]
        public void Test_Generation_DateTimeToken()
        {
            var builder = new PatternBuilder();
            var gen = builder.AddDateTime().GetGenerator();

            var first = gen.GetNext();
            var second = gen.GetNext();

            var firstSub = first[..8];
            var secondSub = second[..8];

            var datetimeValuePrefix = DateTime.Now.ToString("yyMMddHH");

            Assert.AreEqual(datetimeValuePrefix, firstSub);
            Assert.AreEqual(datetimeValuePrefix, secondSub);
        }
    }
}