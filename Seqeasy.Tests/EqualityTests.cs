using Microsoft.VisualStudio.TestTools.UnitTesting;

using Seqeasy.Design;

namespace Seqeasy.Tests
{
    [TestClass]
    public class EqualityTests
    {
        [TestMethod]
        public void Test_Const_Section_Equality()
        {
            var constToken1 = new ConstSectionToken("TEST");
            var constToken2 = new ConstSectionToken("TEST");
            
            Assert.AreEqual(constToken1, constToken2);
        }

        [TestMethod]
        public void Test_Const_Section_Inequality()
        {
            var constToken1 = new ConstSectionToken("TEST1");
            var constToken2 = new ConstSectionToken("TEST2");

            Assert.AreNotEqual(constToken1, constToken2);
        }

        [TestMethod]
        public void Test_Random_Section_Equality()
        {
            var random1 = new RandomSectionToken("ABCDEF1234", 2, CaseSpecifier.UppercaseOnly);
            var random2 = new RandomSectionToken("ABCDEF1234", 2, CaseSpecifier.UppercaseOnly);

            Assert.AreEqual(random1, random2);
        }

        [TestMethod]
        public void Test_Random_Section_Inequality_On_Charset()
        {
            var random1 = new RandomSectionToken("ABCDEF");
            var random2 = new RandomSectionToken("ABCDEF1234");

            Assert.AreNotEqual(random1, random2);
        }

        [TestMethod]
        public void Test_Random_Section_Inequality_On_Width()
        {
            var random1 = new RandomSectionToken("ABCDEF1234", 2);
            var random2 = new RandomSectionToken("ABCDEF1234", 3);

            Assert.AreNotEqual(random1, random2);
        }

        [TestMethod]
        public void Test_Random_Section_Inequality_On_Case()
        {
            var random1 = new RandomSectionToken("ABCDEF1234", caseSpecifier: CaseSpecifier.UppercaseOnly);
            var random2 = new RandomSectionToken("ABCDEF1234", caseSpecifier: CaseSpecifier.LowercaseOnly);

            Assert.AreNotEqual(random1, random2);
        }
    }
}