﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Seqeasy.Design;

namespace Seqeasy.Tests
{
    [TestClass]
    public class TokenTests
    {
        [TestMethod]
        public void Test_Const_Char_Token()
        {
            var constToken = new ConstSectionToken('A');

            Assert.IsTrue(constToken.Width == 1);
            Assert.AreEqual("A", constToken.ConstValue);
        }

        [TestMethod]
        public void Test_Const_String_Token()
        {
            var constToken = new ConstSectionToken("TESTCONST");

            Assert.IsTrue(constToken.Width == 9);
            Assert.AreEqual("TESTCONST", constToken.ConstValue);
        }


        [TestMethod]
        public void Test_Increment_Token_Defaults()
        {
            var incToken = new IncrementSectionToken("ABCDEF");

            Assert.AreEqual(1, incToken.Width);
            Assert.AreEqual(1, incToken.IncrementAmount);
            Assert.AreEqual(0, incToken.StartValue);
            Assert.AreEqual(1, incToken.ChainId);
        }

        [TestMethod]
        public void Test_Increment_Token()
        {
            var incToken = new IncrementSectionToken("ABCDEF", width: 4, incrementAmount: 2, startValue: 10, chainId: 1);

            Assert.AreEqual("ABCDEF", incToken.CharacterSet);
            Assert.AreEqual(4, incToken.Width);
            Assert.AreEqual(2, incToken.IncrementAmount);
            Assert.AreEqual(10, incToken.StartValue);
            Assert.AreEqual(1, incToken.ChainId);
        }

        [TestMethod]
        public void Test_Random_Letter_Except_Number()
        {
            Assert.ThrowsException<ArgumentException>(() => new RandomLetterSectionToken("12345"));
        }

        [TestMethod]
        public void Test_Random_Letter()
        {
            var randomLetterToken = new RandomLetterSectionToken("ABCDEF", 2, CaseSpecifier.UppercaseOnly);

            Assert.AreEqual("ABCDEF", randomLetterToken.CharacterSet);
            Assert.AreEqual(2, randomLetterToken.Width);
            Assert.AreEqual(CaseSpecifier.UppercaseOnly, randomLetterToken.CaseSpecifier);
        }

        [TestMethod]
        public void Test_Random_Letter_Defaults()
        {
            var randomLetterToken = new RandomLetterSectionToken("ABCDEF");

            Assert.AreEqual("ABCDEF", randomLetterToken.CharacterSet);
            Assert.AreEqual(1, randomLetterToken.Width);
            Assert.AreEqual(CaseSpecifier.Unspecified, randomLetterToken.CaseSpecifier);
        }

        [TestMethod]
        public void Test_Random_Number_Except_Letter()
        {
            Assert.ThrowsException<ArgumentException>(() => new RandomNumberSectionToken("ABCDEF"));
        }

        [TestMethod]
        public void Test_Random_Number()
        {
            var randomNumberToken = new RandomNumberSectionToken("123456789", 2);

            Assert.AreEqual("123456789", randomNumberToken.CharacterSet);
            Assert.AreEqual(2, randomNumberToken.Width);
        }

        [TestMethod]
        public void Test_Random_Number_Defaults()
        {
            var randomNumberToken = new RandomNumberSectionToken("123456789");

            Assert.AreEqual("123456789", randomNumberToken.CharacterSet);
            Assert.AreEqual(1, randomNumberToken.Width);
        }
    }
}