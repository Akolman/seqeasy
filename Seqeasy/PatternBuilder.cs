﻿using System;
using System.Collections.Generic;
using System.Numerics;

using Seqeasy.CharacterSets;
using Seqeasy.Design;

namespace Seqeasy
{
    /// <summary>
    /// Optional flags for <see cref="PatternBuilder"/> instances.
    /// </summary>
    [Flags]
    public enum PatternBuilderOptions
    {
        /// <summary>
        /// None.  The default option.
        /// </summary>
        None = 0,

        /// <summary>
        /// Skip combination calculation.
        /// </summary>
        SkipComboCalculation = 1
    }

    /// <summary>
    /// Represents a generic PatternBuilder.
    /// </summary>
    public class PatternBuilder
    {
        /// <summary>
        /// Gets or sets the options for this <see cref="PatternBuilder"/>.
        /// </summary>
        protected PatternBuilderOptions builderOptions = PatternBuilderOptions.None;

        /// <summary>
        /// Initializes a new instance of the <see cref="PatternBuilder"/> class.
        /// </summary>
        public PatternBuilder()
        {
            Tokens = new List<SectionToken>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PatternBuilder"/> class with options.
        /// </summary>
        public PatternBuilder(PatternBuilderOptions options)
            : this()
        {
            builderOptions = options;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PatternBuilder"/> class
        /// with default <see cref="SectionToken"/> set.
        /// </summary>
        /// <param name="sectionTokens">The set of SectionTokens to initialize with this PatternBuilder.</param>
        public PatternBuilder(ICollection<SectionToken> sectionTokens)
        {
            Tokens = new List<SectionToken>(sectionTokens);
        }

        /// <summary>
        /// Gets or sets the set of SectionTokens for this PatternBuilder.
        /// </summary>
        public ICollection<SectionToken> Tokens { get; set; }

        /// <summary>
        /// Gets this builder's current <see cref="Pattern"/>.
        /// </summary>
        /// <returns>The Pattern.</returns>
        public Pattern GetPattern()
        {
            var width = 0;
            BigInteger combos = 1;
            List<SectionToken> tokens = new(Tokens.Count);

            foreach (var token in Tokens)
            {
                var copyToke = token with { };

                if (copyToke is ISizedSectionToken patternToken)
                {
                    if (width == 0)
                    {
                        width = patternToken.Width;
                    }
                    else
                    {
                        width += patternToken.Width;
                    }
                }
                else
                {
                    width = int.MaxValue;
                }

                if (!builderOptions.HasFlag(PatternBuilderOptions.SkipComboCalculation))
                {
                    if (copyToke is ISizedVariableCharSetSectionToken charsetToken)
                    {
                        combos *= BigInteger.Pow(charsetToken.CharacterSet.Length, charsetToken.Width);
                    }
                    else if (copyToke is IFiniteCombinationSectionToken finiteToken)
                    {
                        combos *= finiteToken.TotalCombinations;
                    }
                }
                else
                {
                    combos = -1;
                }

                tokens.Add(copyToke);
            }

            return new Pattern(tokens, width, combos);
        }

        /// <summary>
        /// Returns the width and combinations statistics for the currently-built <see cref="Pattern"/>.
        /// </summary>
        /// <returns>The width and number of combinations the current pattern as built can produce.</returns>
        public (int Width, BigInteger Combinations) GetDetails()
        {
            var tempPattern = GetPattern();
            return (tempPattern.Width, tempPattern.Combinations);
        }

        /// <summary>
        /// Adds a <see cref="SectionToken"/> to the current builder.
        /// </summary>
        /// <param name="token">The SectionToken to add.</param>
        /// <returns>This PatternBuilder.</returns>
        public PatternBuilder AddToken(SectionToken token)
        {
            Tokens.Add(token);
            return this;
        }

        /// <summary>
        /// Adds a set of <see cref="SectionToken"/> to the current builder.
        /// </summary>
        /// <param name="tokens">The SectionTokens.</param>
        /// <returns>This PatternBuilder.</returns>
        public PatternBuilder AddTokens(SectionToken[] tokens)
        {
            foreach (var token in tokens)
            {
                AddToken(token);
            }

            return this;
        }

        /// <summary>
        /// Adds a constant character section to the builder.
        /// </summary>
        /// <param name="constCharValue">The character constant to add.</param>
        /// <returns>This PatternBuilder.</returns>
        public PatternBuilder AddConst(char constCharValue)
        {
            return AddToken(new ConstSectionToken(constCharValue));
        }

        /// <summary>
        /// Adds a constant string section to the builder.
        /// </summary>
        /// <param name="constStringValue">The string constant to add.</param>
        /// <returns>This PatternBuilder.</returns>
        public PatternBuilder AddConst(string constStringValue)
        {
            return AddToken(new ConstSectionToken(constStringValue));
        }

        /// <summary>
        /// Adds a random symbol section to the builder, with options.
        /// </summary>
        /// <param name="charset">The set of characters to use.</param>
        /// <param name="width">The width of this section.</param>
        /// <returns>This PatternBuilder.</returns>
        public PatternBuilder AddSymbol(string charset, int width = 1)
        {
            return AddToken(new RandomSymbolSectionToken(charset, width));
        }

        /// <summary>
        /// Adds a random character section to the builder, with options.
        /// </summary>
        /// <param name="charset">The set of characters to use.</param>
        /// <param name="width">The width of this section.</param>
        /// <param name="caseSpecifier">Case specifier setting.</param>
        /// <returns>This PatternBuilder.</returns>
        public PatternBuilder AddChar(string charset, int width = 1, CaseSpecifier caseSpecifier = CaseSpecifier.Unspecified)
        {
            return AddToken(new RandomSectionToken(charset, width, caseSpecifier));
        }

        /// <summary>
        /// Adds a random letter section to the builder, with options.
        /// </summary>
        /// <param name="charset">The set of characters to use.</param>
        /// <param name="width">The width of this section.</param>
        /// <param name="caseSpecifier">Case specifier setting.</param>
        /// <returns>This PatternBuilder.</returns>
        public PatternBuilder AddLetter(string charset, int width = 1, CaseSpecifier caseSpecifier = CaseSpecifier.Unspecified)
        {
            return AddToken(new RandomLetterSectionToken(charset, width, caseSpecifier));
        }

        /// <summary>
        /// Adds a random number section to the builder, with options.
        /// </summary>
        /// <param name="charset">The set of number characters to use.</param>
        /// <param name="width">The width of this section.</param>
        /// <returns>This PatternBuilder.</returns>
        public PatternBuilder AddNumber(string charset, int width = 1)
        {
            return AddToken(new RandomNumberSectionToken(charset, width));
        }

        /// <summary>
        /// Adds an increment section to the builder, with options.
        /// </summary>
        /// <param name="charset">The set of characters to use.</param>
        /// <param name="width">The width of this section.</param>
        /// <param name="incrementAmount">The amount to increment each generation.</param>
        /// <param name="startValue">The starting value for this increment section.</param>
        /// <param name="chainId">The ID of the chain that links this increment section.</param>
        /// <returns>This PatternBuilder.</returns>
        public PatternBuilder AddIncrement(string charset, int width = 1, int incrementAmount = 1, int startValue = 0, int chainId = 1)
        {
            return AddToken(new IncrementSectionToken(charset, width, incrementAmount, startValue, chainId));
        }

        /// <summary>
        /// Adds a <see cref="DateTimeSectionToken"/> with optional format.
        /// See <see cref="DateTime.ToString(string?)"/> for format info.
        /// </summary>
        /// <param name="format">Optional format specifier.</param>
        /// <returns>The current builder.</returns>
        public PatternBuilder AddDateTime(string format = "yyMMddHHmmssffff")
        {
            return AddToken(new DateTimeSectionToken(format));
        }

        /// <summary>
        /// Produces a <see cref="SequenceGenerator"/> from this <see cref="PatternBuilder"/>.
        /// </summary>
        /// <param name="generatorOptions">Optional GeneratorOptions for the SequenceGenerator.</param>
        /// <returns>This PatternBuilder.</returns>
        public SequenceGenerator GetGenerator(GeneratorOptions? generatorOptions = null)
        {
            return SequenceGenerator.Build(GetPattern(), generatorOptions ?? GeneratorOptions.Defaults);
        }
    }

    /// <summary>
    /// Represents a <see cref="PatternBuilder"/> with default <see cref="CharacterSet"/>.
    /// </summary>
    /// <typeparam name="T">The default CharacterSet to apply to all tokens added.</typeparam>
    public class PatternBuilder<T> : PatternBuilder
        where T : CharacterSet, new()
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PatternBuilder{T}"/> class
        /// using default charset T.
        /// </summary>
        /// <example>
        /// <code>
        /// using Seqeasy.CharacterSets.Special;
        ///
        /// var builder = new PatternBuilder&lt;HexCharSet&gt;();
        /// </code>
        /// </example>
        public PatternBuilder()
            : base()
        {
            DefaultCharSet = new T();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PatternBuilder{T}"/> class
        /// using default charset T and options.
        /// </summary>
        /// <example>
        /// <code>
        /// using Seqeasy.CharacterSets.Special;
        ///
        /// var builder = new PatternBuilder&lt;HexCharSet&gt;(PatternBuilderOptions.SkipComboCalculation);
        /// </code>
        /// </example>
        public PatternBuilder(PatternBuilderOptions options)
            : this()
        {
            builderOptions = options;
        }

        /// <summary>
        /// Gets the chosen <see cref="CharacterSet"/> instance for this PatternBuilder.
        /// </summary>
        public CharacterSet DefaultCharSet { get; }

        /// <summary>
        /// Adds a random letter section to the builder.
        /// </summary>
        /// <param name="width">The width, in characters, of this section.</param>
        /// <param name="caseSpecifier">Optional case specifier.</param>
        /// <returns>This PatternBuilder.</returns>
        public PatternBuilder<T> AddLetter(int width = 1, CaseSpecifier caseSpecifier = CaseSpecifier.Unspecified)
        {
            AddLetter(DefaultCharSet.Letters, width, caseSpecifier);
            return this;
        }

        /// <summary>
        /// Adds a random number section to the builder.
        /// </summary>
        /// <param name="width">The width, in characters, of this section.</param>
        /// <returns>This PatternBuilder.</returns>
        public PatternBuilder<T> AddNumber(int width = 1)
        {
            AddNumber(DefaultCharSet.Numbers, width);
            return this;
        }

        /// <summary>
        /// Adds a random symbol section to the builder.
        /// </summary>
        /// <param name="width">The width, in characters, of this section.</param>
        /// <returns>This PatternBuilder.</returns>
        public PatternBuilder<T> AddSymbol(int width = 1)
        {
            AddSymbol(DefaultCharSet.Symbols, width);
            return this;
        }

        /// <summary>
        /// Adds a random character section to the builder.
        /// </summary>
        /// <param name="width">The width, in characters, of this section.</param>
        /// <param name="caseSpecifier">Optional case specifier.</param>
        /// <returns>This PatternBuilder.</returns>
        public PatternBuilder<T> AddChar(int width = 1, CaseSpecifier caseSpecifier = CaseSpecifier.Unspecified)
        {
            AddChar(DefaultCharSet.Chars, width, caseSpecifier);
            return this;
        }

        /// <summary>
        /// Adds an increment section to the builder.
        /// </summary>
        /// <param name="width">The width of this section.</param>
        /// <param name="incrementAmount">The increment amount.</param>
        /// <param name="startValue">The start value.</param>
        /// <param name="chainId">The ID of the chain linking this incrementer.</param>
        /// <returns>This PatternBuilder.</returns>
        public PatternBuilder<T> AddIncrement(int width = 1, int incrementAmount = 1, int startValue = 0, int chainId = 1)
        {
            AddIncrement(DefaultCharSet.Chars, width, incrementAmount, startValue, chainId);
            return this;
        }

        /// <summary>
        /// Adds a const section.
        /// </summary>
        /// <param name="constCharVal">The constant character value.</param>
        /// <returns>This PatternBuilder.</returns>
        public new PatternBuilder<T> AddConst(char constCharVal)
        {
            base.AddConst(constCharVal);
            return this;
        }

        /// <summary>
        /// Adds a const section.
        /// </summary>
        /// <param name="constStringVal">The constant character value.</param>
        /// <returns>This PatternBuilder.</returns>
        public new PatternBuilder<T> AddConst(string constStringVal)
        {
            base.AddConst(constStringVal);
            return this;
        }

        /// <summary>
        /// Adds a random character section to the builder, with options.
        /// </summary>
        /// <param name="charset">The set of characters to use.</param>
        /// <param name="width">The width of this section.</param>
        /// <param name="caseSpecifier">Case specifier setting.</param>
        /// <returns>This PatternBuilder.</returns>
        public new PatternBuilder<T> AddChar(string charset, int width = 1, CaseSpecifier caseSpecifier = CaseSpecifier.Unspecified)
        {
            base.AddChar(charset, width, caseSpecifier);
            return this;
        }

        /// <summary>
        /// Adds a random letter section to the builder, with options.
        /// </summary>
        /// <param name="charset">The set of characters to use.</param>
        /// <param name="width">The width of this section.</param>
        /// <param name="caseSpecifier">Case specifier setting.</param>
        /// <returns>This PatternBuilder.</returns>
        public new PatternBuilder<T> AddLetter(string charset, int width = 1, CaseSpecifier caseSpecifier = CaseSpecifier.Unspecified)
        {
            base.AddLetter(charset, width, caseSpecifier);
            return this;
        }

        /// <summary>
        /// Adds a random number section to the builder, with options.
        /// </summary>
        /// <param name="charset">The set of number characters to use.</param>
        /// <param name="width">The width of this section.</param>
        /// <returns>This PatternBuilder.</returns>
        public new PatternBuilder<T> AddNumber(string charset, int width = 1)
        {
            base.AddNumber(charset, width);
            return this;
        }

        /// <summary>
        /// Adds an increment section to the builder, with options.
        /// </summary>
        /// <param name="charset">The set of characters to use.</param>
        /// <param name="width">The width of this section.</param>
        /// <param name="incrementAmount">The amount to increment each generation.</param>
        /// <param name="startValue">The starting value for this increment section.</param>
        /// <param name="chainId">The ID of the chain that links this increment section.</param>
        /// <returns>This PatternBuilder.</returns>
        public new PatternBuilder<T> AddIncrement(string charset, int width = 1, int incrementAmount = 1, int startValue = 0, int chainId = 1)
        {
            base.AddIncrement(charset, width, incrementAmount, startValue, chainId);
            return this;
        }

        /// <summary>
        /// Adds a <see cref="DateTimeSectionToken"/> with optional format.
        /// See <see cref="DateTime.ToString(string?)"/> for format info.
        /// </summary>
        /// <param name="format">Optional format specifier.</param>
        /// <returns>The current builder.</returns>
        public new PatternBuilder<T> AddDateTime(string format = "yyMMddHHmmssffff")
        {
            base.AddDateTime(format);
            return this;
        }
    }
}