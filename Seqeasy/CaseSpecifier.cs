namespace Seqeasy
{
    /// <summary>
    /// Specifier for case setting used when generating patterns which contain letters.
    /// </summary>
    public enum CaseSpecifier
    {
        /// <summary>
        /// Unspecified - leave up to the charset.
        /// </summary>
        Unspecified = 0,

        /// <summary>
        /// Uppercase letters only.
        /// </summary>
        UppercaseOnly = 1,

        /// <summary>
        /// Lowercase letters only.
        /// </summary>
        LowercaseOnly = 2,
    }
}