﻿using System;

namespace Seqeasy
{
    /// <summary>
    /// Defines the random-ness level to use.
    /// </summary>
    public enum RandomizerLevel
    {
        /// <summary>
        /// Use a standard random number generator.
        /// </summary>
        Standard = 0,

        /// <summary>
        /// Use a cryptographically-secure random number generator.
        /// </summary>
        Secure = 1,
    }

    /// <summary>
    /// Defines the thread-safety model.
    /// </summary>
    public enum ThreadSafetyModel
    {
        /// <summary>
        /// The generator will not be thread-safe.
        /// </summary>
        NonThreadSafe = 0,

        /// <summary>
        /// The generator will be thread-safe.  Incurs time cost for locks.
        /// </summary>
        ThreadSafe = 1,
    }

    /// <summary>
    /// Defines the set of options available for a <see cref="SequenceGenerator"/>.
    /// </summary>
    public class GeneratorOptions
    {
        /// <summary>
        /// Gets a default set of <see cref="GeneratorOptions"/>.
        /// </summary>
        public static GeneratorOptions Defaults => new();

        /// <summary>
        /// Gets or sets the <see cref="ThreadSafetyModel"/> value.
        /// </summary>
        public ThreadSafetyModel ThreadSafetyModel { get; set; } = ThreadSafetyModel.NonThreadSafe;

        /// <summary>
        /// Gets or sets the <see cref="RandomizerLevel"/> value.
        /// </summary>
        public RandomizerLevel RandomizerLevel { get; set; } = RandomizerLevel.Standard;

        /// <summary>
        /// Gets or sets a value indicating whether increment chains should be configured for start position.
        /// </summary>
        public bool ConfigureChains { get; set; } = true;

        /// <summary>
        /// Gets or sets a value indicating whether the RNG initialization should be skipped.
        /// </summary>
        public bool SkipInitRandom { get; set; } = false;

        /// <summary>
        /// Gets or sets a value indicating whether an overflow will result in an exception (true)
        /// or a reset of the incrementer (false).
        /// </summary>
        public bool ThrowOnOverflow { get; set; } = true;
    }
}
