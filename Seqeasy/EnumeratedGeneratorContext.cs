﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

namespace Seqeasy
{
    /// <summary>
    /// Wrapper object for enumerating a <see cref="SequenceGenerator"/>.
    /// </summary>
    public class EnumeratedGeneratorContext : IEnumerable<string>
    {
        private readonly int count;
        private readonly SequenceGenerator generator;
        private readonly BatchGenerationOptions generationOptions;

        /// <summary>
        /// Initializes a new instance of the <see cref="EnumeratedGeneratorContext"/> class.
        /// </summary>
        /// <param name="sequenceGenerator">The SequenceGenerator to enumerate.</param>
        /// <param name="generationCount">The number of sequences to generate.</param>
        /// <param name="options">Set of BatchGenerationOptions.</param>
        internal EnumeratedGeneratorContext(SequenceGenerator sequenceGenerator, int generationCount, BatchGenerationOptions options)
        {
            count = generationCount;
            generator = sequenceGenerator;
            generationOptions = options;
        }

        /// <summary>
        /// Gets an enumerator.
        /// </summary>
        /// <returns>A string enumerator.</returns>
        public IEnumerator<string> GetEnumerator()
        {
            var e = GetGeneratorEnumerator();

            while (e.MoveNext())
            {
                yield return (string)e.Current;
            }
        }

        /// <summary>
        /// Gets an enumerator.
        /// </summary>
        /// <returns>An enumerator.</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetGeneratorEnumerator();
        }

        private GeneratorEnumerator GetGeneratorEnumerator()
        {
            if (generationOptions.HasFlag(BatchGenerationOptions.Deduplicate))
            {
                return new DedupingGeneratorEnumerator(generator, count);
            }
            else
            {
                return new GeneratorEnumerator(generator, count);
            }
        }
    }

    /// <summary>
    /// Basic (non-deduplicating) enumerator wrapper.
    /// </summary>
    internal class GeneratorEnumerator : IEnumerator
    {
        private readonly SequenceGenerator generator;
        private readonly int generationCount;
        private int counter = 0;

        private string? current;

        /// <summary>
        /// Initializes a new instance of the <see cref="GeneratorEnumerator"/> class.
        /// </summary>
        /// <param name="sg">The SequenceGenerator to enumerate.</param>
        /// <param name="count">The number of sequences to generate.</param>
        public GeneratorEnumerator(SequenceGenerator sg, int count)
        {
            generator = sg;
            generationCount = count;
        }

        /// <inheritdoc/>
        public object Current
        {
            get
            {
                if (current == null)
                {
                    MoveNext();
                }

                Debug.Assert(current != null, "Unable to move to first");

                return current;
            }
        }

        /// <inheritdoc/>
        public bool MoveNext()
        {
            if (++counter > generationCount)
            {
                return false;
            }

            current = GetGeneratorValue();
            return true;
        }

        /// <inheritdoc/>
        public virtual void Reset()
        {
            counter = 0;
        }

        /// <summary>
        /// Produces the next generator value.
        /// </summary>
        /// <returns>The next sequence value.</returns>
        protected virtual string GetGeneratorValue()
        {
            return generator.GetNext();
        }
    }

    /// <summary>
    /// Deduplicating enumerator wrapper.
    /// </summary>
    internal sealed class DedupingGeneratorEnumerator : GeneratorEnumerator
    {
        private readonly HashSet<string> produced;

        /// <summary>
        /// Initializes a new instance of the <see cref="DedupingGeneratorEnumerator"/> class.
        /// </summary>
        /// <param name="sg">The SequenceGenerator to enumerate.</param>
        /// <param name="count">The number of sequences to generate.</param>
        public DedupingGeneratorEnumerator(SequenceGenerator sg, int count)
            : base(sg, count)
        {
            if (sg.Context.Pattern.Combinations < (ulong)count)
            {
                throw new ArgumentException($"Specified '{nameof(count)}' ({count}) exceeds total possible combinations that can be produced ({sg.Context.Pattern.Combinations}).  Deduplication not possible.");
            }

            produced = new(count);
        }

        /// <inheritdoc/>
        public override void Reset()
        {
            produced.Clear();
            base.Reset();
        }

        /// <summary>
        /// Gets the next sequence value ensure deduplication.
        /// </summary>
        /// <returns>The next value.</returns>
        protected override string GetGeneratorValue()
        {
            var next = base.GetGeneratorValue();
            while (produced.Contains(next))
            {
                next = base.GetGeneratorValue();
            }

            produced.Add(next);
            return next;
        }
    }
}