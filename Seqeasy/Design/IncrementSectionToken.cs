﻿using System.Linq;
using System.Text;
using Seqeasy.Generator.Section;

namespace Seqeasy.Design
{
    /// <summary>
    /// Represents a section which increments.
    /// </summary>
    public record IncrementSectionToken : SectionToken, ISizedVariableCharSetSectionToken
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="IncrementSectionToken"/> class.
        /// </summary>
        /// <param name="charSet">The set of characters to use for this incrementer token.</param>
        /// <param name="width">The width of this section in the incrementer.</param>
        /// <param name="incrementAmount">The increment amount.</param>
        /// <param name="startValue">The incrementer start value.</param>
        /// <param name="chainId">The increment chain this incrementer is associated with.</param>
        public IncrementSectionToken(string charSet, int width = 1, int incrementAmount = 1, int startValue = 0, int chainId = 1)
        {
            CharacterSet = charSet;
            Width = width;
            IncrementAmount = incrementAmount;
            StartValue = startValue;
            ChainId = chainId;
        }

        /// <summary>
        /// Gets the amount to increment this section.
        /// </summary>
        public int IncrementAmount { get; }

        /// <summary>
        /// Gets the starting value for this section.
        /// </summary>
        public int StartValue { get; }

        /// <summary>
        /// Gets the width of this section.
        /// </summary>
        public int Width { get; }

        /// <summary>
        /// Gets the set of characters.
        /// </summary>
        public string CharacterSet { get; }

        /// <summary>
        /// Gets the chain ID of this section.
        /// </summary>
        public int ChainId { get; }

        /// <inheritdoc/>
        protected override void AddSection(GeneratorContext generatorContext)
        {
            var chain = generatorContext.Chains.Single(c => c.ChainId.Equals(ChainId));
            for (var i = 0; i < Width; i++)
            {
                generatorContext.Generators.Add(new IncrementSection(this, chain, generatorContext.Options.ThrowOnOverflow));
            }

            if (generatorContext.Options.ConfigureChains)
            {
                chain.SetStartValue(StartValue);
            }
        }
    }
}
