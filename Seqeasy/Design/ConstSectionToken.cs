﻿using Seqeasy.Generator.Section;

namespace Seqeasy.Design
{
    /// <summary>
    /// Represents a constant value in a section.
    /// </summary>
    public record ConstSectionToken : SectionToken, ISizedSectionToken, IFiniteCombinationSectionToken
    {
        private readonly string constValue;

        /// <summary>
        /// Initializes a new instance of the <see cref="ConstSectionToken"/> class.
        /// </summary>
        internal ConstSectionToken()
        {
            constValue = string.Empty;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ConstSectionToken"/> class with defined char.
        /// </summary>
        /// <param name="value">The char constant.</param>
        public ConstSectionToken(char value)
            : this(value.ToString())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ConstSectionToken"/> class with defined string.
        /// </summary>
        /// <param name="value">The string constant.</param>
        public ConstSectionToken(string value)
        {
            constValue = value;
        }

        /// <inheritdoc/>
        public int Width => ConstValue.Length;

        /// <summary>
        /// Gets the const value of this token.
        /// </summary>
        public string ConstValue => constValue;

        /// <inheritdoc/>
        public int TotalCombinations => 1;

        /// <inheritdoc/>
        protected override void AddSection(GeneratorContext generatorContext)
        {
            generatorContext.Generators.Add(new ConstSection(this));
        }
    }
}
