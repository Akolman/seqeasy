﻿namespace Seqeasy.Design
{
    /// <summary>
    /// Represents a token with a variable character set and defined width.
    /// </summary>
    public interface ISizedVariableCharSetSectionToken : ISizedSectionToken
    {
        /// <summary>
        /// Gets the character set of the token.
        /// </summary>
        public abstract string CharacterSet { get; }
    }
}
