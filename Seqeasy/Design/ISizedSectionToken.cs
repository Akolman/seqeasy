﻿namespace Seqeasy.Design
{
    /// <summary>
    /// Represents a token which has a defined size.
    /// </summary>
    public interface ISizedSectionToken
    {
        /// <summary>
        /// Gets the width of the token.
        /// </summary>
        public int Width { get; }
    }
}
