﻿using System;

using Seqeasy.Design;
using Seqeasy.Generator.Section;

namespace Seqeasy.Design
{
    /// <summary>
    /// Represents a DateTime value in a sequence.
    /// </summary>
    public record DateTimeSectionToken : SectionToken, ISizedSectionToken
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DateTimeSectionToken"/> class.
        /// </summary>
        /// <param name="format">The DateTime style format specifier.</param>
        public DateTimeSectionToken(string format = "yyMMddHHmmssffff")
        {
            Format = format;
            Width = DateTime.Now.ToString(Format).Length;
        }

        /// <summary>
        /// Gets the format of this token.
        /// </summary>
        public string Format { get; }

        /// <inheritdoc/>
        public int Width { get; }

        /// <inheritdoc/>
        protected override void AddSection(GeneratorContext generatorContext)
        {
            generatorContext.Generators.Add(new DateTimeSection(Format));
        }
    }
}
