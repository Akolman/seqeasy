﻿using System;

namespace Seqeasy.Design
{
    /// <summary>
    /// Represents a random letter in a section.
    /// </summary>
    public record RandomLetterSectionToken : RandomSectionToken
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RandomLetterSectionToken"/> class.
        /// </summary>
        public RandomLetterSectionToken()
            : base()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RandomLetterSectionToken"/> class.
        /// </summary>
        /// <param name="width">The width of this random section.</param>
        /// <param name="charSet">The set of characters for this random section.</param>
        /// <param name="caseSpecifier">Optional case specifier for this random section.</param>
        public RandomLetterSectionToken(string charSet, int width = 1, CaseSpecifier caseSpecifier = CaseSpecifier.Unspecified)
             : base(charSet, width, caseSpecifier)
        {
        }

        /// <inheritdoc/>
        protected override void Validate(string chars)
        {
            foreach (var ch in chars.ToCharArray())
            {
                if (char.IsDigit(ch))
                {
                    throw new ArgumentException($"{typeof(RandomLetterSectionToken)} cannot contain digit {ch}");
                }
            }
        }
    }
}
