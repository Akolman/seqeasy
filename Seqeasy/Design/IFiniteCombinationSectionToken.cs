﻿using System;

namespace Seqeasy.Design
{
    /// <summary>
    /// Represents a section with a finite combinations that cannot be deduced from the width and charset length.
    /// </summary>
    public interface IFiniteCombinationSectionToken : ISizedSectionToken
    {
        /// <summary>
        /// Gets the total number of combinations this section can produce.
        /// </summary>
        public abstract int TotalCombinations { get; }
    }
}
