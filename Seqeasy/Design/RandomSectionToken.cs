﻿using System;
using System.Text;
using Seqeasy.Generator.Section;

namespace Seqeasy.Design
{
    /// <summary>
    /// Represents a section which produces random values.
    /// </summary>
    public record RandomSectionToken : SectionToken, ISizedVariableCharSetSectionToken
    {
        private string characterSet = string.Empty;

        /// <summary>
        /// Initializes a new instance of the <see cref="RandomSectionToken"/> class.
        /// </summary>
        public RandomSectionToken()
        {
            Width = 0;
            CharacterSet = string.Empty;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RandomSectionToken"/> class.
        /// </summary>
        /// <param name="charSet">The set of characters to use for this section.</param>
        /// <param name="width">The width, in chars, of this section.</param>
        /// <param name="caseSpecifier">Optional case specifier.</param>
        public RandomSectionToken(string charSet, int width = 1, CaseSpecifier caseSpecifier = CaseSpecifier.Unspecified)
        {
            if (string.IsNullOrWhiteSpace(charSet))
            {
                throw new ArgumentNullException(nameof(charSet));
            }
            else if (width < 1)
            {
                throw new ArgumentOutOfRangeException(nameof(width));
            }

            Width = width;
            CharacterSet = charSet;
            CaseSpecifier = caseSpecifier;
        }

        /// <summary>
        /// Gets the width of this random section.
        /// </summary>
        public int Width { get; }

        /// <summary>
        /// Gets the <see cref="CaseSpecifier"/> setting for
        /// this random section.
        /// </summary>
        public CaseSpecifier CaseSpecifier { get; }

        /// <summary>
        /// Gets the character set for this random section.
        /// Setting will call <see cref="Validate(string)">Validate</see>.
        /// </summary>
        public string CharacterSet
        {
            get => characterSet;
            private set
            {
                Validate(value);
                characterSet = value;
            }
        }

        /// <summary>
        /// Over-ridable method used to validate characters set to <see cref="CharacterSet"/>.
        /// </summary>
        /// <param name="chars">The characters to validate.</param>
        protected virtual void Validate(string chars)
        {
        }

        /// <inheritdoc/>
        protected override void AddSection(GeneratorContext generatorContext)
        {
            if (generatorContext.Randomizer == null)
            {
                throw new InvalidOperationException($"{nameof(GeneratorContext.Randomizer)} not initialized");
            }

            generatorContext.Generators.Add(new RandomSection(this, generatorContext.Randomizer));
        }
    }
}
