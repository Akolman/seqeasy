﻿using System;

namespace Seqeasy.Design
{
    /// <summary>
    /// Represents a random number section.
    /// </summary>
    public record RandomNumberSectionToken : RandomSectionToken
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RandomNumberSectionToken"/> class.
        /// </summary>
        /// <param name="width">The width of this random number section.</param>
        /// <param name="charSet">The set of number characters to use for this random number section.</param>
        public RandomNumberSectionToken(string charSet, int width = 1)
            : base(charSet, width)
        {
        }

        /// <inheritdoc/>
        protected override void Validate(string chars)
        {
            foreach (var c in chars.ToCharArray())
            {
                if (!char.IsNumber(c))
                {
                    throw new ArgumentException($"{typeof(RandomNumberSectionToken)} cannot have character {c}");
                }
            }
        }
    }
}
