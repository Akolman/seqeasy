﻿namespace Seqeasy.Design
{
    /// <summary>
    /// Represents the base class for all section type implementations.
    /// </summary>
    public abstract record SectionToken
    {
        /// <summary>
        /// Called internally to invoke <see cref="AddSection(GeneratorContext)"/>.
        /// </summary>
        /// <param name="generatorContext">The GeneratorContext to add this token to.</param>
        internal void AddSectionInternal(GeneratorContext generatorContext)
        {
            AddSection(generatorContext);
        }

        /// <summary>
        /// Adds the current token to a <see cref="GeneratorContext"/>.
        /// </summary>
        /// <param name="generatorContext">The GeneratorContext to add this token to.</param>
        protected abstract void AddSection(GeneratorContext generatorContext);
    }
}
