﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Runtime.CompilerServices;
using System.Text;
using Seqeasy.Design;

[assembly: InternalsVisibleTo("Seqeasy.StringPattern")]
[assembly: InternalsVisibleTo("Seqeasy.Extensions")]

namespace Seqeasy
{
    /// <summary>
    /// Represents a set of tokens produced by a <see cref="PatternBuilder"/>.
    /// </summary>
    public class Pattern
    {
        private readonly IEnumerable<SectionToken> tokens;

        /// <summary>
        /// Initializes a new instance of the <see cref="Pattern"/> class.
        /// </summary>
        /// <param name="sectionTokens">An array of SectionTokens used to initialize this Pattern.</param>
        public Pattern(IEnumerable<SectionToken> sectionTokens)
        {
            var pattern = new PatternBuilder(sectionTokens.ToList()).GetPattern();
            tokens = pattern.CopyTokens();
            Width = pattern.Width;
            Combinations = pattern.Combinations;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Pattern"/> class.
        /// </summary>
        /// <param name="sectionTokens">An array of SectionTokens used to initialize this Pattern.</param>
        /// <param name="width">The width of this Pattern.</param>
        /// <param name="combinations">The number of combinations this pattern can generate.</param>
        internal Pattern(IEnumerable<SectionToken> sectionTokens, int width, BigInteger combinations)
        {
            tokens = new List<SectionToken>(sectionTokens);
            Width = width;
            Combinations = combinations;
        }

        private Pattern()
        {
            tokens = new List<SectionToken>();
            Width = 0;
            Combinations = 0;
        }

        /// <summary>
        /// Gets a copy of this pattern's tokens.
        /// </summary>
        public IEnumerable<SectionToken> Tokens => tokens;

        /// <summary>
        /// Gets this pattern's width.
        /// </summary>
        public int Width { get; }

        /// <summary>
        /// Gets this pattern's number of combinations.
        /// </summary>
        public BigInteger Combinations { get; }

        /// <summary>
        /// Returns a copy of this Pattern's tokens.
        /// </summary>
        /// <returns>A coyp of this Pattern's SectionToken[].</returns>
        public ICollection<SectionToken> CopyTokens()
        {
            var newList = new List<SectionToken>(tokens.Count());

            foreach (var token in tokens)
            {
                newList.Add(token with { });
            }

            return newList;
        }

        /// <summary>
        /// Returns a copy of this Pattern.
        /// </summary>
        /// <returns>A copy of this.</returns>
        public Pattern Copy()
        {
            return new Pattern(CopyTokens(), Width, Combinations);
        }

        /// <inheritdoc/>
        public override int GetHashCode()
        {
            return tokens.GetHashCode();
        }

        /// <inheritdoc/>
        public override bool Equals(object? obj)
        {
            if (obj is not Pattern pattern)
            {
                return false;
            }

            return tokens.SequenceEqual(pattern.tokens);
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <returns>A debug string of this pattern.</returns>
        public override string ToString()
        {
            var sb = new StringBuilder(90 * tokens.Count());
            var idx = 1;
            foreach (var token in tokens)
            {
                sb.Append($"Token{idx++}: ");
                sb.AppendLine(token.ToString());
            }

            return sb.ToString();
        }
    }
}
