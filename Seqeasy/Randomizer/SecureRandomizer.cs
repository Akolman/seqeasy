﻿using System;
using System.Security.Cryptography;

namespace Seqeasy.Randomizer
{
    /// <summary>
    /// Represents a cryptographically-secure RNG.
    /// </summary>
    public class SecureRandomizer : IRandomizer
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SecureRandomizer"/> class.
        /// </summary>
        internal SecureRandomizer()
        {
        }

        /// <inheritdoc/>
        public int Next(int max)
        {
            return RandomNumberGenerator.GetInt32(max);
        }
    }
}
