﻿using System;

namespace Seqeasy.Randomizer
{
    /// <summary>
    /// Represents a regular, non crypto-secure RNG.
    /// </summary>
    internal class StandardRandomizer : IRandomizer
    {
        private readonly Random random = new(DateTime.Now.Millisecond);

        /// <inheritdoc/>
        public int Next(int max)
        {
            return random.Next(max);
        }
    }
}
