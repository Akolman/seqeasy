﻿namespace Seqeasy.Randomizer
{
    /// <summary>
    /// Interface the represents an object that produces random numbers.
    /// </summary>
    public interface IRandomizer
    {
        /// <summary>
        /// Produces the next random value.
        /// </summary>
        /// <param name="max">The max int in the range to produce.</param>
        /// <returns>A random value.</returns>
        public int Next(int max);
    }
}
