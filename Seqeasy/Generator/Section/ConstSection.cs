using System.Text;

using Seqeasy.Design;

namespace Seqeasy.Generator.Section
{
    /// <summary>
    /// Section representing a constant value in a sequence.
    /// </summary>
    public class ConstSection : IGeneratorSection
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ConstSection"/> class.
        /// </summary>
        /// <param name="constSection">A ConstSectionToken used to create this ConstSection.</param>
        internal ConstSection(ConstSectionToken constSection)
        {
            Value = constSection.ConstValue;
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        public int Id { get; set; } = 0;

        /// <summary>
        /// Gets or sets the value of this ConstSection.
        /// </summary>
        public string Value { get; set; }

        /// <inheritdoc/>
        void IGeneratorSection.AppendNextSection(StringBuilder sb)
        {
            sb.Append(Value);
        }
    }
}
