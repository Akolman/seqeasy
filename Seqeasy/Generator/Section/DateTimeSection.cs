﻿using System;
using System.Text;

using Seqeasy.Generator;

namespace Seqeasy.Generator.Section
{
    /// <summary>
    /// Represents a DateTime value in the section.
    /// </summary>
    public class DateTimeSection : IGeneratorSection
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DateTimeSection"/> class.
        /// </summary>
        /// <param name="format">The DateTime style format specifier.</param>
        internal DateTimeSection(string format)
        {
            if (string.IsNullOrWhiteSpace(format))
            {
                throw new ArgumentNullException(nameof(format));
            }

            Format = format;
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        public int Id { get; set; } = 0;

        /// <summary>
        /// Gets or sets the format for this DateTimeSection.
        /// </summary>
        public string Format { get; set; }

        /// <inheritdoc/>
        void IGeneratorSection.AppendNextSection(StringBuilder sb)
        {
            sb.Append(DateTime.Now.ToString(Format));
        }
    }
}
