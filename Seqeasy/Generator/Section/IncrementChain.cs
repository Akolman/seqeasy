﻿using System.Collections.Generic;
using System.Linq;

namespace Seqeasy.Generator.Section
{
    /// <summary>
    /// Represents a connector for <see cref="IncrementSection"/> section instances.
    /// </summary>
    public class IncrementChain
    {
        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the ID of this IncrementChain.
        /// </summary>
        public int ChainId { get; set; }

        /// <summary>
        /// Gets or sets the list of IncrementSections linked by this chain.
        /// </summary>
        public List<IncrementSection> LinkedSections { get; set; } = new();

        /// <summary>
        /// Produces a new list of IncrementChains given an array of chain IDs.
        /// </summary>
        /// <param name="ids">The chain IDs for which to create IncrementChains for.</param>
        /// <returns>A list of ID'd IncrementChains.</returns>
        public static List<IncrementChain> New(int[] ids)
        {
            var rtn = new List<IncrementChain>(ids.Length);
            foreach (var id in ids)
            {
                rtn.Add(new IncrementChain()
                {
                    ChainId = id,
                });
            }

            return rtn;
        }

        /// <summary>
        /// Adds an <see cref="IncrementSection"/> to the current <see cref="IncrementChain"/>.
        /// </summary>
        /// <param name="section">The section to add.</param>
        public void AddSection(IncrementSection section)
        {
            LinkedSections.Add(section);
        }

        /// <summary>
        /// Increments the value(s) in this <see cref="IncrementChain"/>.
        /// </summary>
        public void Increment()
        {
            LinkedSections.Last().Increment();
        }

        /// <summary>
        /// Gets the <see cref="IncrementSection"/> one order up from the provided section.
        /// Returns null if this is the end section and no section up the chain exists.
        /// </summary>
        /// <param name="section">The section from which to locate.</param>
        /// <returns>The section one order up relative to the provided section.</returns>
        internal IncrementSection? NextSection(IncrementSection section)
        {
            var nextIndex = LinkedSections.IndexOf(section) - 1;
            if (nextIndex < 0)
            {
                return null;
            }

            return LinkedSections[nextIndex];
        }

        /// <summary>
        /// Sets the sections according to the start value 'value' of the chain.
        /// </summary>
        /// <param name="value">The starting value for the chain.</param>
        internal void SetStartValue(int value)
        {
            for (var i = LinkedSections.Count - 1; i >= 0; i--)
            {
                var section = LinkedSections[i];

                if (value < 0 || value > section.CharSet.Length)
                {
                    section.StartValue = 0;
                    section.CurrentValue = 0;
                }
                else
                {
                    section.StartValue = value;
                    section.CurrentValue = value;
                    value -= section.CharSet.Length;
                }
            }

            LinkedSections.Last().CurrentValue -= 1;
        }
    }
}
