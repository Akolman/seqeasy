﻿using System.Text;

using Seqeasy.Design;
using Seqeasy.Randomizer;

namespace Seqeasy.Generator.Section
{
    /// <summary>
    /// Represents a section which produces a random character.
    /// </summary>
    public class RandomSection : IGeneratorSection
    {
        private readonly IRandomizer random;

        /// <summary>
        /// Initializes a new instance of the <see cref="RandomSection"/> class.
        /// </summary>
        /// <param name="randomToken">T RandomSectionToken used to produce this RandomSection.</param>
        /// <param name="randomizer">An IRandomizer instance used to produce random values.</param>
        internal RandomSection(RandomSectionToken randomToken, IRandomizer randomizer)
        {
            Charset = randomToken.CharacterSet;
            Width = randomToken.Width;
            random = randomizer;
            CaseSpecifier = randomToken.CaseSpecifier;
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        public int Id { get; set; } = 0;

        /// <summary>
        /// Gets or sets the <see cref="CaseSpecifier"/> value for this RandomSection.
        /// </summary>
        public CaseSpecifier CaseSpecifier { get; set; }

        /// <summary>
        /// Gets or sets the set of characters used by this RandomSection.
        /// </summary>
        public string Charset { get; set; }

        /// <summary>
        /// Gets or sets the width, in chars, of this section.
        /// </summary>
        public int Width { get; set; }

        /// <inheritdoc/>
        void IGeneratorSection.AppendNextSection(StringBuilder sb)
        {
            for (var i = 0; i < Width; i++)
            {
                var theChar = Charset[random.Next(Charset.Length)];
                if (CaseSpecifier == CaseSpecifier.Unspecified)
                {
                    sb.Append(theChar);
                }
                else if (CaseSpecifier == CaseSpecifier.LowercaseOnly)
                {
                    sb.Append(char.ToLower(theChar));
                }
                else
                {
                    sb.Append(char.ToUpper(theChar));
                }
            }
        }
    }
}
