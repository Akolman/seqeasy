﻿using System.Linq;
using System.Text;

using Seqeasy.Design;

namespace Seqeasy.Generator.Section
{
    /// <summary>
    /// Represents a section which increments a given value on each iteration.
    /// </summary>
    public class IncrementSection : IGeneratorSection
    {
        private readonly IOverflowHandler overflowHandler;

        /// <summary>
        /// Initializes a new instance of the <see cref="IncrementSection"/> class.
        /// </summary>
        /// <param name="incrementToken">An Increment token used to create this IncrementSection.</param>
        /// <param name="chain">The IncrementChain that links this IncrementSection.</param>
        /// <param name="throwOnOverflow">Whether to throw on overflow.</param>
        public IncrementSection(IncrementSectionToken incrementToken, IncrementChain chain, bool throwOnOverflow)
        {
            IncrementChain = chain;
            CharSet = incrementToken.CharacterSet;
            StartValue = incrementToken.StartValue;
            CurrentValue = incrementToken.StartValue;
            IncrementAmount = incrementToken.IncrementAmount;

            if (throwOnOverflow)
            {
                overflowHandler = new ThrowingOverflowHandler();
            }
            else
            {
                overflowHandler = new ResettingOverflowHandler();
            }

            chain.AddSection(this);
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        public int Id { get; set; } = 0;

        /// <summary>
        /// Interface which represents a handler for overflow events.
        /// </summary>
        private interface IOverflowHandler
        {
            /// <summary>
            /// Handles the current overflow.
            /// </summary>
            /// <param name="incrementSection">The section.</param>
            public void HandleSectionOverflow(IncrementSection incrementSection);
        }

        /// <summary>
        /// Gets or sets the <see cref="IncrementChain"/> that links this <see cref="IncrementSection"/>.
        /// </summary>
        public IncrementChain IncrementChain { get; set; }

        /// <summary>
        /// Gets or sets the set of characters that this <see cref="IncrementSection"/> uses.
        /// </summary>
        public string CharSet { get; set; }

        /// <summary>
        /// Gets or sets the increment amount for this <see cref="IncrementSection"/>.
        /// </summary>
        public int IncrementAmount { get; set; }

        /// <summary>
        /// Gets or sets the starting value for this <see cref="IncrementSection"/>.
        /// </summary>
        public int StartValue { get; set; }

        /// <summary>
        /// Gets or sets the current value for this <see cref="IncrementSection"/>.
        /// </summary>
        public int CurrentValue { get; set; }

        /// <inheritdoc/>
        void IGeneratorSection.AppendNextSection(StringBuilder sb)
        {
            sb.Append(CharSet[CurrentValue]);
        }

        /// <summary>
        /// Increments this section and rolls to the next if this section has reached it's overflow.
        /// </summary>
        internal void Increment()
        {
            if (CurrentValue + IncrementAmount >= CharSet.Length)
            {
                overflowHandler.HandleSectionOverflow(this);
            }
            else
            {
                CurrentValue += IncrementAmount;
            }
        }

        private class ThrowingOverflowHandler : IOverflowHandler
        {
            public void HandleSectionOverflow(IncrementSection incrementSection)
            {
                incrementSection.CurrentValue -= incrementSection.CharSet.Length + (incrementSection.IncrementAmount * -1);

                var next = incrementSection.IncrementChain.NextSection(incrementSection)
                    ?? throw new System.OverflowException("Incrementer has overflown");

                next.Increment();
            }
        }

        private class ResettingOverflowHandler : IOverflowHandler
        {
            public void HandleSectionOverflow(IncrementSection incrementSection)
            {
                incrementSection.CurrentValue -= incrementSection.CharSet.Length + (incrementSection.IncrementAmount * -1);

                var next = incrementSection.IncrementChain.NextSection(incrementSection);

                /* Will be null when rolling the last section in the incrementer*/
                if (next == null)
                {
                    incrementSection.IncrementChain.Increment();
                    incrementSection.IncrementChain.LinkedSections.Last().CurrentValue -= 1;
                }
                else
                {
                    next.Increment();
                }
            }
        }
    }
}
