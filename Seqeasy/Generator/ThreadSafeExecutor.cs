﻿namespace Seqeasy.Generator
{
    /// <summary>
    /// Represents a thread-safe executor.
    /// </summary>
    internal class ThreadSafeExecutor : IGeneratorExecutor
    {
        private readonly object locker = new();

        /// <summary>
        /// Initializes a new instance of the <see cref="ThreadSafeExecutor"/> class.
        /// </summary>
        internal ThreadSafeExecutor()
        {
        }

        /// <inheritdoc/>
        public string Invoke(SequenceGenerator generator)
        {
            lock (locker)
            {
                return IGeneratorExecutor.Produce(generator);
            }
        }
    }
}
