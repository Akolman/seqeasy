﻿using System;
using System.Text;

namespace Seqeasy.Generator
{
    /// <summary>
    /// Interface that represents a section generator.
    /// </summary>
    public interface IGeneratorSection
    {
        /// <summary>
        /// Gets the ID of this <see cref="IGeneratorSection"/> within the parent context.
        /// </summary>
        public int Id { get; internal set; }

        /// <summary>
        /// Called internally to executed <see cref="AppendNextSection(StringBuilder)"/>.
        /// </summary>
        /// <param name="sb">The StringBuilder to append to.</param>
        internal void InvokeAppend(StringBuilder sb) => AppendNextSection(sb);

        /// <summary>
        /// Appends a section's next value to a <see cref="StringBuilder"/>.
        /// </summary>
        /// <param name="sb">The current value being built.</param>
        protected void AppendNextSection(StringBuilder sb);
    }
}
