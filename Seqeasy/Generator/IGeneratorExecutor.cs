﻿using System.Text;

namespace Seqeasy.Generator
{
    /// <summary>
    /// Interface that represents a model for executing a generator.
    /// </summary>
    internal interface IGeneratorExecutor
    {
        /// <summary>
        /// Starts the executor on the next sequence.
        /// </summary>
        /// <param name="generator">The generator to be invoked.</param>
        /// <returns>The next sequence value.</returns>
        public string Invoke(SequenceGenerator generator);

        /// <summary>
        /// Static logic which will iterate through each generator in the sequence and return the generated value.
        /// </summary>
        /// <param name="generator">The generator used to produce a sequence.</param>
        /// <returns>The next sequence.</returns>
        protected static string Produce(SequenceGenerator generator)
        {
            var len = (generator.Context.Pattern.Width < int.MaxValue) ? generator.Context.Pattern.Width : 64;
            var sb = new StringBuilder(len);
            generator.Context.Chains.ForEach(c => c.Increment());
            generator.Context.Generators.ForEach(gen => gen.InvokeAppend(sb));
            return sb.ToString();
        }
    }
}
