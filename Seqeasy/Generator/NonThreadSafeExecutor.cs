﻿namespace Seqeasy.Generator
{
    /// <summary>
    /// Represents a non-thread safe executor.
    /// </summary>
    internal class NonThreadSafeExecutor : IGeneratorExecutor
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NonThreadSafeExecutor"/> class.
        /// </summary>
        internal NonThreadSafeExecutor()
        {
        }

        /// <inheritdoc/>
        public string Invoke(SequenceGenerator generator)
        {
            return IGeneratorExecutor.Produce(generator);
        }
    }
}
