﻿using System.Collections.Generic;
using System.Linq;

using Seqeasy.Design;
using Seqeasy.Generator;
using Seqeasy.Generator.Section;
using Seqeasy.Randomizer;

namespace Seqeasy
{
    /// <summary>
    /// Represents a wrapper for a <see cref="SequenceGenerator"/> instance information.
    /// </summary>
    public class GeneratorContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GeneratorContext"/> class.
        /// </summary>
        /// <param name="options">A set of options used by the generator.</param>
        /// <param name="pattern">The pattern the generator uses to produce sequences.</param>
        internal GeneratorContext(GeneratorOptions options, Pattern pattern)
        {
            Pattern = pattern;
            Options = options;
            Generators = new();
            Executor = ProduceExecutor(Options);
            Chains = IncrementChain.New(PrepChains(Pattern));

            if (!options.SkipInitRandom)
            {
                Randomizer = ProduceRandomizer(Options);
            }
        }

        /// <summary>
        /// Gets the <see cref="Pattern"/> used to create this <see cref="GeneratorContext"/>.
        /// </summary>
        public Pattern Pattern { get; private set; }

        /// <summary>
        /// Gets the <see cref="IRandomizer"/> for this context.
        /// </summary>
        public IRandomizer? Randomizer { get; private set; }

        /// <summary>
        /// Gets the list of <see cref="IGeneratorSection"/> instances for this context.
        /// </summary>
        public List<IGeneratorSection> Generators { get; private set; }

        /// <summary>
        /// Gets the list of <see cref="IncrementChain"/> instances for this context.
        /// </summary>
        public List<IncrementChain> Chains { get; private set; }

        /// <summary>
        /// Gets the <see cref="GeneratorOptions"/> for this context.
        /// </summary>
        public GeneratorOptions Options { get; private set; }

        /// <summary>
        /// Gets the <see cref="IGeneratorExecutor"/> for this context.
        /// </summary>
        internal IGeneratorExecutor Executor { get; private set; }

        private static int[] PrepChains(Pattern pattern)
        {
            return pattern.Tokens.Where(t => t is IncrementSectionToken).Select(t => (t as IncrementSectionToken)!.ChainId).Distinct().ToArray();
        }

        private static IGeneratorExecutor ProduceExecutor(GeneratorOptions options)
        {
            if (options.ThreadSafetyModel == ThreadSafetyModel.NonThreadSafe)
            {
                return new NonThreadSafeExecutor();
            }
            else
            {
                return new ThreadSafeExecutor();
            }
        }

        private static IRandomizer ProduceRandomizer(GeneratorOptions options)
        {
            if (options.RandomizerLevel == RandomizerLevel.Standard)
            {
                return new StandardRandomizer();
            }
            else
            {
                return new SecureRandomizer();
            }
        }
    }
}
