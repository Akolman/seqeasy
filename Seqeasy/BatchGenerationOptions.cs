﻿using System;

namespace Seqeasy
{
    /// <summary>
    /// Set of options used with <see cref="SequenceGenerator.GetBatch(int, BatchGenerationOptions)"/>.
    /// </summary>
    [Flags]
    public enum BatchGenerationOptions
    {
        /// <summary>
        /// No options.
        /// </summary>
        None = 0,

        /// <summary>
        /// Ensure deduplication of batch-generated sequences.
        /// </summary>
        Deduplicate = 1,
    }
}
