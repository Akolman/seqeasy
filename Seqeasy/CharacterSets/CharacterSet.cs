﻿using System;
using System.Linq;
using System.Text;

namespace Seqeasy.CharacterSets
{
    /// <summary>
    /// Provides the base class for all pre-defined CharacterSet implementations.
    /// </summary>
    public abstract class CharacterSet
    {
        private string? letters;
        private string? numbers;
        private string? symbols;

        /// <summary>
        /// Gets the set of characters.
        /// </summary>
        public abstract string Chars { get; }

        /// <summary>
        /// Gets the ID of the character set.
        /// </summary>
        public abstract int CharacterSetId { get; }

        /// <summary>
        /// Gets the set of letter characters from the character set.
        /// </summary>
        public virtual string Letters
        {
            get
            {
                if (letters == null)
                {
                    letters = GetLetters(Chars);
                }

                return letters;
            }
        }

        /// <summary>
        /// Gets the set of number characters from the character set.
        /// </summary>
        public virtual string Numbers
        {
            get
            {
                if (numbers == null)
                {
                    numbers = GetNumbers(Chars);
                }

                return numbers;
            }
        }

        /// <summary>
        /// Gets the set of symbol characters from the character set.
        /// </summary>
        public virtual string Symbols
        {
            get
            {
                if (symbols == null)
                {
                    symbols = GetSymbols(Chars);
                }

                return symbols;
            }
        }

        /// <summary>
        /// Gets a string which holds all letter characters from the given set of characters 'chars'.
        /// </summary>
        /// <param name="chars">The set of characters to get letters from.</param>
        /// <returns>The set of letter characters.</returns>
        internal static string GetLetters(string chars)
            => GetCharacterBy(chars, char.IsLetter);

        /// <summary>
        /// Gets a string which holds all number characters from the given set of characters 'chars'.
        /// </summary>
        /// <param name="chars">The set of characters to get numbers from.</param>
        /// <returns>The set of number characters.</returns>
        internal static string GetNumbers(string chars)
            => GetCharacterBy(chars, char.IsNumber);

        internal static string GetSymbols(string chars)
            => GetCharacterBy(chars, (c) => char.IsSymbol(c) || char.IsPunctuation(c));

        private static string GetCharacterBy(string chars, Func<char, bool> func)
        {
            return new string(chars.Where(func).ToArray());
        }
    }
}
