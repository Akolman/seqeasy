﻿using System;

namespace Seqeasy.CharacterSets.CaseSensitive
{
    /// <summary>
    /// Represents a full set of alphanumeric characters with symbols.
    /// </summary>
    public sealed class AlphaNumFullSymbols : CharacterSet
    {
        /// <summary>
        /// Gets the set of characters.
        /// </summary>
        public override string Chars => @"0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ~`!@#$%^&*()_-+={[}]|\:;""'<,>.?/";

        /// <inheritdoc/>
        public override int CharacterSetId => 2118;
    }
}
