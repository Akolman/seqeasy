﻿namespace Seqeasy.CharacterSets.CaseSensitive
{
    /// <summary>
    /// Represents a case sensitive (uppercase and lowercase) set of all alphanumeric characters with lowercase L
    /// and uppercase I and O removed
    /// (0123456789abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ).
    /// </summary>
    public sealed class AlphaNumNoAmbigLetters : CharacterSet
    {
        /// <summary>
        /// Gets the set of characters.
        /// </summary>
        public override string Chars => "0123456789abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ";

        /// <inheritdoc/>
        public override int CharacterSetId => 2120;
    }
}
