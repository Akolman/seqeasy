﻿namespace Seqeasy.CharacterSets.CaseSensitive
{
    /// <summary>
    /// Defines case-sensitive (uppercase and lowercase) character sets.
    /// </summary>
    [System.Runtime.CompilerServices.CompilerGeneratedAttribute]
#pragma warning disable SA1400 // Access modifier should be declared
    class NamespaceDoc
#pragma warning restore SA1400 // Access modifier should be declared
    {
    }
}
