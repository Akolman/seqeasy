﻿namespace Seqeasy.CharacterSets.CaseSensitive
{
    /// <summary>
    /// Represents a case sensitive (uppercase and lowercase) set of all alphanumeric characters
    /// (0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ).
    /// </summary>
    public sealed class AlphaNumFull : CharacterSet
    {
        /// <summary>
        /// Gets the set of characters.
        /// </summary>
        public override string Chars => "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

        /// <inheritdoc/>
        public override int CharacterSetId => 2110;
    }
}
