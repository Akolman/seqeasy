﻿namespace Seqeasy.CharacterSets.CaseInsensitive
{
    /// <summary>
    /// Represents a case insensitive set of all alphanumeric characters (0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ).
    /// </summary>
    public sealed class AlphaNumFull : CharacterSet
    {
        /// <summary>
        /// Gets the set of characters.
        /// </summary>
        public override string Chars => "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        /// <inheritdoc/>
        public override int CharacterSetId => 1110;
    }
}
