﻿namespace Seqeasy.CharacterSets.CaseInsensitive
{
    /// <summary>
    /// Defines case-insensitive (uppercase only) character sets.
    /// </summary>
    [System.Runtime.CompilerServices.CompilerGeneratedAttribute]
#pragma warning disable SA1400 // Access modifier should be declared
    class NamespaceDoc
#pragma warning restore SA1400 // Access modifier should be declared
    {
    }
}
