﻿namespace Seqeasy.CharacterSets.CaseInsensitive.International
{
    /// <summary>
    /// Represents a set of case insensitive Greek characters (0123456789ΑΒΓΔΕΖΗΘΙΚΛΜΝΞΟΠΡΣΤΥΦΧΨΩ).
    /// </summary>
    public sealed class GreekFull : CharacterSet
    {
        /// <summary>
        /// Gets the set of characters.
        /// </summary>
        public override string Chars => "0123456789ΑΒΓΔΕΖΗΘΙΚΛΜΝΞΟΠΡΣΤΥΦΧΨΩ";

        /// <inheritdoc/>
        public override int CharacterSetId => 1310;
    }
}
