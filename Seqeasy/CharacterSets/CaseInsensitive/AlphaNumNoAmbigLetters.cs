﻿namespace Seqeasy.CharacterSets.CaseInsensitive
{
    /// <summary>
    /// Represents a case insensitive set of all alphanumeric characters with I and O removed (0123456789ABCDEFGHJKLMNPQRSTUVWXYZ).
    /// </summary>
    public sealed class AlphaNumNoAmbigLetters : CharacterSet
    {
        /// <summary>
        /// Gets the set of characters.
        /// </summary>
        public override string Chars => "0123456789ABCDEFGHJKLMNPQRSTUVWXYZ";

        /// <inheritdoc/>
        public override int CharacterSetId => 1120;
    }
}
