﻿namespace Seqeasy.CharacterSets
{
    /// <summary>
    /// Represents a user-defined character set.
    /// </summary>
    public class GenericCharacterSet
    {
        private readonly string chars;

        private GenericCharacterSet(string charSet)
        {
            chars = charSet;
        }

        /// <summary>
        /// Gets the set of characters for this GenericCharacterSet.
        /// </summary>
        public string Chars => chars;

        /// <summary>
        /// Produces a <see cref="GenericCharacterSet">GenericCharacterSet</see> from the set of characters in 'chars'.
        /// </summary>
        /// <param name="chars">The set of characters used for this GenericCharacterSet.</param>
        /// <returns>The GenericCharacterSet.</returns>
        public static GenericCharacterSet From(string chars)
        {
            return new GenericCharacterSet(chars);
        }
    }
}
