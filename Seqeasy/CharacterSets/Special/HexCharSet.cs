﻿namespace Seqeasy.CharacterSets.Special
{
    /// <summary>
    /// Represents a character set which uses all hexadecimal characters (0-9, A-F).
    /// </summary>
    public class HexCharSet : CharacterSet
    {
        /// <summary>
        /// Gets the set of characters.
        /// </summary>
        public override string Chars => "0123456789ABCDEF";

        /// <inheritdoc/>
        public override int CharacterSetId => 3016;
    }
}
