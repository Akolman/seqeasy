﻿namespace Seqeasy.CharacterSets.Special
{
    /// <summary>
    /// Represents a character set which uses all hexadecimal characters (0-9, A-F).
    /// </summary>
    public class BinaryCharSet : CharacterSet
    {
        /// <summary>
        /// Gets the set of characters.
        /// </summary>
        public override string Chars => "01";

        /// <inheritdoc/>
        public override int CharacterSetId => 3001;
    }
}
