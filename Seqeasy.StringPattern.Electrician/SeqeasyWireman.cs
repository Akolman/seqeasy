﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using System.Diagnostics;
using Microsoft.CodeAnalysis.Text;

namespace Seqeasy.StringPattern.Electrician
{
    [Generator]
    public class SeqeasyWireman : ISourceGenerator
    {
        private string GetTokenType(ClassDeclarationSyntax tokenClass)
        {
            return (((tokenClass.BaseList.Types[0] as SimpleBaseTypeSyntax).Type
                as GenericNameSyntax).TypeArgumentList.Arguments.First()
                as IdentifierNameSyntax).Identifier.Text;
        }

        private bool IsUsingCharsetConstructor(ClassDeclarationSyntax tokenClass)
        {
            var constructorInfo = tokenClass.Members.Where(m => m is ConstructorDeclarationSyntax).Cast<ConstructorDeclarationSyntax>();
            foreach(var constructor in constructorInfo)
            {
                if(constructor.ParameterList.Parameters.Count > 0)
                {
                    return true;
                }
            }
            
            return false;
        }

        private string WriteWireTokenTextReturnTokenType(ClassDeclarationSyntax tokenClass, GeneratorExecutionContext context)
        {
            var tokenType = GetTokenType(tokenClass);
            var wiretext = $@"using System;

using Seqeasy.Design;
using Seqeasy.StringPattern;
using Seqeasy.StringPattern.Converter;

namespace Seqeasy.StringPattern
{{
    public static partial class Wirings
    {{
        /// <summary>
        /// Auto-generated.
        /// </summary>
        internal class {tokenType}Converter : IConverterWiring
        {{
            private readonly ITokenConverter<{tokenType}> converter;

            private Exception? lastException;

            /// <summary>
            /// <inheritdoc/>
            /// </summary>
            public {tokenType}Converter()
            {{
                converter = new {tokenClass.Identifier}();
            }}

            /// <summary>
            /// Initializes a new instance of the <see cref=""{tokenType}Converter""/> class.
            /// </summary>
            public {tokenType}Converter(CharacterSets.CharacterSet characterSet)
            {{
                converter = new {tokenClass.Identifier}({(IsUsingCharsetConstructor(tokenClass) ? "characterSet" : string.Empty)});
            }}

            /// <summary>
            /// <inheritdoc/>
            /// </summary>
            public Exception? LastException => lastException;

            /// <summary>
            /// <inheritdoc/>
            /// </summary>
            public char[] Identifiers => converter.TokenIdentifiers;

            /// <summary>
            /// <inheritdoc/>
            /// </summary>
            public Type TokenType => typeof({tokenType});



            /// <summary>
            /// <inheritdoc/>
            /// </summary>
            public string GetStringPattern(SectionToken token)
            {{
                return converter.GetStringPattern((token as {tokenType})!);
            }}

            /// <summary>
            /// <inheritdoc/>
            /// </summary>
            public (int Index, SectionToken? Token) Parse(char[] chars, int index)
            {{
                try
                {{
                    return (converter.Parse(chars, index, out {tokenType} token), (SectionToken)token);
                }}
                catch (Exception e)
                {{
                    lastException = e;
                    return (index, null);
                }}
            }}
        }}
    }}
}}
";
            context.AddSource($"Wirings.{tokenType}.cs", SourceText.From(wiretext, Encoding.UTF8));
            return tokenType;
        }

        private void WriteStaticWirings(GeneratorExecutionContext context, List<string> converters)
        {
            var wdsrc = $@"using System;
using System.Collections.Generic;
using Seqeasy.Design;

namespace Seqeasy.StringPattern
{{
    /// <summary>
    /// Auto-generated.
    /// Defines internal types.
    /// </summary>
    public static partial class Wirings
    {{
        internal static List<Type> DiscoveredConverters {{ get; }} = new List<Type>({converters.Count()})
        {{";

            foreach (var t in converters)
            {
                wdsrc += $@"
            typeof({t}Converter)";

                if (converters.Last() != t)
                {
                    wdsrc += ',';
                }
            }
            wdsrc += '\n';
            wdsrc += $@"        }};
    }}
 }}
";

            context.AddSource($"Wirings.Static.cs", wdsrc);
        }

        public void Execute(GeneratorExecutionContext context)
        {
            var syntaxReceiver = (MySyntaxReceiver)context.SyntaxReceiver;

            var userClasses = syntaxReceiver.ClassesToAugment;
            if (userClasses is null || userClasses.Count == 0)
            {
                return;
            }

            var converters = new List<string>();
            foreach(var userClass in userClasses)
            {
               // var idProp = userClass.Members.Where(m => m is PropertyDeclarationSyntax).First() as PropertyDeclarationSyntax;
                //var identifier = (char)(idProp.ExpressionBody.Expression as LiteralExpressionSyntax).Token.Value;
                converters.Add(WriteWireTokenTextReturnTokenType(userClass, context));

            }

            WriteStaticWirings(context, converters);

            return;
        }

        public void Initialize(GeneratorInitializationContext context)
        {
            context.RegisterForSyntaxNotifications(() => new MySyntaxReceiver());
        }

        private class MySyntaxReceiver : ISyntaxReceiver
        {
            public IList<ClassDeclarationSyntax> ClassesToAugment { get; private set; }

            public MySyntaxReceiver()
            {
                //Debugger.Launch();
                ClassesToAugment = new List<ClassDeclarationSyntax>();
            }

            public void OnVisitSyntaxNode(SyntaxNode syntaxNode)
            {
                if(syntaxNode is ClassDeclarationSyntax cds
                    && cds.BaseList != null)
                {
                    foreach(var type in cds.BaseList.Types)
                    {
                        if(type.Type is GenericNameSyntax gns &&
                            gns.Identifier.Text == "ITokenConverter")
                        {
                            
                            ClassesToAugment.Add(cds);
                            return;
                        }
                    }
                }
            }
        }
    }
}
