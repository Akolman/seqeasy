using Microsoft.VisualStudio.TestTools.UnitTesting;

using Seqeasy;
using Seqeasy.StringPattern;
using Seqeasy.CharacterSets.Special;

namespace Seqeasy.StringPattern.Tests
{
    [TestClass]
    public class StringPatternTests
    {
        [TestMethod]
        public void Test_Const_StringPattern()
        {
            var builder = new PatternBuilder<HexCharSet>();
            var stringPattern = builder.AddConst("CONST").GetStringPattern();

            Assert.AreEqual("[CONST]", stringPattern);
        }

        [TestMethod]
        public void Test_Random_StringPattern()
        {
            var builder = new PatternBuilder<HexCharSet>();
            var stringPattern = builder.AddChar(2).GetStringPattern();

            Assert.AreEqual("R2", stringPattern);
        }

        [TestMethod]
        public void Test_Random_Uppercase_StringPattern()
        {
            var builder = new PatternBuilder<HexCharSet>();
            var stringPattern = builder.AddChar(2, CaseSpecifier.UppercaseOnly).GetStringPattern();

            Assert.AreEqual("R2(-1)", stringPattern);
        }

        [TestMethod]
        public void Test_Random_Lowercase_StringPattern()
        {
            var builder = new PatternBuilder<HexCharSet>();
            var stringPattern = builder.AddChar(2, CaseSpecifier.LowercaseOnly).GetStringPattern();

            Assert.AreEqual("R2(-2)", stringPattern);
        }

        [TestMethod]
        public void Test_Random_Letter_StringPattern()
        {
            var builder = new PatternBuilder<HexCharSet>();
            var stringPattern = builder.AddLetter(2).GetStringPattern();

            Assert.AreEqual("L2", stringPattern);
        }

        [TestMethod]
        public void Test_Random_Letter_Uppercase_StringPattern()
        {
            var builder = new PatternBuilder<HexCharSet>();
            var stringPattern = builder.AddLetter(2, CaseSpecifier.UppercaseOnly).GetStringPattern();

            Assert.AreEqual("L2(-1)", stringPattern);
        }

        [TestMethod]
        public void Test_Random_Letter_Lowercase_StringPattern()
        {
            var builder = new PatternBuilder<HexCharSet>();
            var stringPattern = builder.AddLetter(2, CaseSpecifier.LowercaseOnly).GetStringPattern();

            Assert.AreEqual("L2(-2)", stringPattern);
        }

        [TestMethod]
        public void Test_Random_Number_StringPattern()
        {
            var builder = new PatternBuilder<HexCharSet>();
            var stringPattern = builder.AddNumber(2).GetStringPattern();

            Assert.AreEqual("N2", stringPattern);
        }

        [TestMethod]
        public void Test_Random_Symbol_StringPattern()
        {
            var builder = new PatternBuilder<CharacterSets.CaseSensitive.AlphaNumFullSymbols>();
            var stringPattern = builder.AddSymbol(2).GetStringPattern();

            Assert.AreEqual("S2", stringPattern);
        }

        [TestMethod]
        public void Test_Increment_FourWidth_StringPattern()
        {
            var builder = new PatternBuilder<HexCharSet>();
            var stringPattern = builder.AddIncrement(4).GetStringPattern();

            Assert.AreEqual("I4", stringPattern);
        }

        [TestMethod]
        public void Test_Increment_TwoWidth_PositiveFourInc_StringPattern()
        {
            var builder = new PatternBuilder<HexCharSet>();
            var stringPattern = builder.AddIncrement(2, 4).GetStringPattern();

            Assert.AreEqual("I2(+4)", stringPattern);
        }

        [TestMethod]
        public void Test_Increment_TwoWidth_NegativeFourInc_StringPattern()
        {
            var builder = new PatternBuilder<HexCharSet>();
            var stringPattern = builder.AddIncrement(2, -4).GetStringPattern();

            Assert.AreEqual("I2(-4)", stringPattern);
        }

        [TestMethod]
        public void Test_Increment_TwoWidth_FiveStartValue_StringPattern()
        {
            var builder = new PatternBuilder<HexCharSet>();
            var stringPattern = builder.AddIncrement(2, startValue: 5).GetStringPattern();

            Assert.AreEqual("I2(-s 5)", stringPattern);
        }

        [TestMethod]
        public void Test_Increment_TwoWidth_TwoChain_StringPattern()
        {
            var builder = new PatternBuilder<HexCharSet>();
            var stringPattern = builder.AddIncrement(2, chainId: 2).GetStringPattern();

            Assert.AreEqual("I2(-c 2)", stringPattern);
        }

        [TestMethod]
        public void Test_Increment_Complex_StringPattern()
        {
            var builder = new PatternBuilder<HexCharSet>();
            var stringPattern = builder.AddIncrement(4, 4, 2, 2).GetStringPattern();

            Assert.AreEqual("I4(+4 -s 2 -c 2)", stringPattern);
        }

        [TestMethod]
        public void Test_DateTime_StringPattern()
        {
            var builder = new PatternBuilder();
            var stringPattern = builder.AddConst("C").AddDateTime().GetStringPattern();

            Assert.AreEqual("[C]D", stringPattern);
        }
    }
}