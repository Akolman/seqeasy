﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

using System.Linq;

using Seqeasy;
using Seqeasy.StringPattern;
using Seqeasy.CharacterSets.Special;
using static Seqeasy.StringPattern.Exceptions;

namespace Seqeasy.StringPattern.Tests
{
    [TestClass]
    public class ParseTests
    {

        [TestMethod]
        public void Parse_Const_StringPattern()
        {
            var pattern = StringPattern.Parse<HexCharSet>("[CONST]");
            var token = pattern.Tokens.First() as Design.ConstSectionToken;

            Assert.IsNotNull(token, "Token for some reason not ConstSectionToken");

            Assert.AreEqual("CONST", token.ConstValue);
            Assert.AreEqual(1, token.TotalCombinations);
        }

        [TestMethod]
        public void Parse_Random_StringPattern()
        {
            var pattern = StringPattern.Parse<HexCharSet>("R");
            var token = pattern.Tokens.First() as Design.RandomSectionToken;

            Assert.IsNotNull(token, "Token for some reason not RandomSectionToken");

            Assert.AreEqual(1, token.Width);
            Assert.AreEqual(CaseSpecifier.Unspecified, token.CaseSpecifier);
        }

        [TestMethod]
        public void Parse_Random_Lowercase_StringPattern()
        {
            var pattern = StringPattern.Parse<HexCharSet>("R20(-2)");
            var token = pattern.Tokens.First() as Design.RandomSectionToken;

            Assert.IsNotNull(token, "Token for some reason not RandomSectionToken");

            Assert.AreEqual(20, token.Width);
            Assert.AreEqual(CaseSpecifier.LowercaseOnly, token.CaseSpecifier);
        }

        [TestMethod]
        public void Parse_Random_Letter_StringPattern()
        {
            var pattern = StringPattern.Parse<HexCharSet>("L2");
            var token = pattern.Tokens.First() as Design.RandomLetterSectionToken;

            Assert.IsNotNull(token, "Token for some reason not RandomLetterSectionToken");

            Assert.AreEqual(2, token.Width);
            Assert.AreEqual(CaseSpecifier.Unspecified, token.CaseSpecifier);
        }

        [TestMethod]
        public void Parse_Random_Letter_Lowercase_By_Letter_StringPattern()
        {
            var pattern = StringPattern.Parse<HexCharSet>("ll");
            var token = pattern.Tokens.ElementAt(1) as Design.RandomLetterSectionToken;

            Assert.IsNotNull(token, "Token for some reason not RandomLetterSectionToken");

            Assert.AreEqual(1, token.Width);
            Assert.AreEqual(CaseSpecifier.LowercaseOnly, token.CaseSpecifier);
        }

        [TestMethod]
        public void Parse_Random_Letter_Uppercase_StringPattern()
        {
            var pattern = StringPattern.Parse<HexCharSet>("L2(-1)");
            var token = pattern.Tokens.First() as Design.RandomLetterSectionToken;

            Assert.IsNotNull(token, "Token for some reason not RandomLetterSectionToken");

            Assert.AreEqual(2, token.Width);
            Assert.AreEqual(CaseSpecifier.UppercaseOnly, token.CaseSpecifier);
        }

        [TestMethod]
        public void Parse_Random_Number_StringPattern()
        {
            var pattern = StringPattern.Parse<HexCharSet>("N");
            var token = pattern.Tokens.First() as Design.RandomNumberSectionToken;

            Assert.IsNotNull(token, "Token for some reason not RandomNumberSectionToken");

            Assert.AreEqual(1, token.Width);
        }

        [TestMethod]
        public void Parse_Random_Number_TwoN_StringPattern()
        {
            var pattern = StringPattern.Parse<HexCharSet>("NN");
            var token = pattern.Tokens.ElementAt(1) as Design.RandomNumberSectionToken;

            Assert.IsNotNull(token, "Token for some reason not RandomNumberSectionToken");

            Assert.AreEqual(1, token.Width);
        }

        [TestMethod]
        public void Parse_Random_Number_N2_StringPattern()
        {
            var pattern = StringPattern.Parse<HexCharSet>("N2");
            var token = pattern.Tokens.First() as Design.RandomNumberSectionToken;

            Assert.IsNotNull(token, "Token for some reason not RandomNumberSectionToken");

            Assert.AreEqual(2, token.Width);
        }

        [TestMethod]
        public void Parse_Random_Symbol_StringPattern()
        {
            var pattern = StringPattern.Parse<CharacterSets.CaseSensitive.AlphaNumFullSymbols>("S2");
            var token = pattern.Tokens.First() as Design.RandomSymbolSectionToken;

            Assert.IsNotNull(token, "Token for some reason not RandomSymbolSectionToken");

            Assert.AreEqual(2, token.Width);
        }

        [TestMethod]
        public void Parse_Increment_StringPattern()
        {
            var pattern = StringPattern.Parse<HexCharSet>("I4");
            var token = pattern.Tokens.First() as Design.IncrementSectionToken;

            Assert.IsNotNull(token, "Token for some reason not IncrementSectionToken");

            Assert.AreEqual(4, token.Width);
            Assert.AreEqual(0, token.StartValue);
            Assert.AreEqual(1, token.IncrementAmount);
            Assert.AreEqual(1, token.ChainId);
        }

        [TestMethod]
        public void Parse_Increment_With_Options_StringPattern()
        {
            var pattern = StringPattern.Parse<HexCharSet>("I2(-s 2 -i 2)");
            var token = pattern.Tokens.First() as Design.IncrementSectionToken;

            Assert.IsNotNull(token, "Token for some reason not IncrementSectionToken");

            Assert.AreEqual(2, token.Width);
            Assert.AreEqual(2, token.StartValue);
            Assert.AreEqual(2, token.IncrementAmount);
            Assert.AreEqual(1, token.ChainId);
        }

        [TestMethod]
        public void Parse_Date_StringPattern()
        {
            var pattern = StringPattern.Parse<HexCharSet>("D");
            var token = pattern.Tokens.First() as Design.DateTimeSectionToken;

            Assert.IsNotNull(token, "Token for some reason not DateTimeSectionToken");
        }

        [TestMethod]
        public void Parse_Date_With_DayPattern_StringPattern()
        {
            var pattern = StringPattern.Parse<HexCharSet>("D(-f YYYYMMDD)");
            var token = pattern.Tokens.First() as Design.DateTimeSectionToken;

            Assert.IsNotNull(token, "Token for some reason not DateTimeSectionToken");

            Assert.AreEqual("YYYYMMDD", token.Format);
        }

        [TestMethod]
        public void Big_Parse_StringPattern()
        {
            var charSet = new HexCharSet();

            var pattern = StringPattern.Parse<HexCharSet>("[CO]R2R(-1)L3I4(-s 4)D");
            var tokens = pattern.Tokens;

            Assert.AreEqual(new Design.ConstSectionToken("CO"), tokens.ElementAt(0));
            Assert.AreEqual(new Design.RandomSectionToken(charSet.Chars, 2), tokens.ElementAt(1));
            Assert.AreEqual(new Design.RandomSectionToken(charSet.Chars, caseSpecifier: CaseSpecifier.UppercaseOnly), tokens.ElementAt(2));
            Assert.AreEqual(new Design.RandomLetterSectionToken(charSet.Letters, width: 3), tokens.ElementAt(3));
            Assert.AreEqual(new Design.IncrementSectionToken(charSet.Chars, width: 4, startValue: 4), tokens.ElementAt(4));
            Assert.IsTrue(tokens.ElementAt(5) is Design.DateTimeSectionToken);
        }

        [TestMethod]
        public void Test_Unknown_Identifier_Throws()
        {
            Assert.ThrowsException<UnknownIdentifierException>(() => StringPattern.Parse<HexCharSet>("_"));
        }

        [TestMethod]
        public void Test_Bad_Expressions_Throws()
        {
            Assert.ThrowsException<MalformedStringPatternException>(() => StringPattern.Parse<HexCharSet>("R4(a"));
        }
    }
}