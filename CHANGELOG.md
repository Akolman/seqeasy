# Seqeasy Changelog

## 0.0.0.2 (2022-03-19)

### Seqeasy
**Updated**
* Changed `IGenerator` to `IGeneratorSection` (#4)

### Seqeasy.StringPattern
**Added**
* Completed `Seqeasy.StringPattern` although testing remains

## 0.0.1

### Seqeasy
**Added**
* Section ID to `IGeneratorSection` types
* Added `Pattern.ToString` override

**Modified**
* Moved `DateTimeToken` to from Extensions to core

### Seqeasy.Extensions
**Added**
* Added `GetGeneratorContext` static method

**Modified**
* Moved `DateTimeToken` to from Extensions to core

### Seqeasy.StringPattern
**Added**
* Added `DateTimeToken` parser

## 0.0.1.2

### Seqeasy.StringPattern
**Added**
* Documentation

## 0.0.1.3

### Seqeasy
**Added**
New `PatternBuilderOptions` for use with `PatternBuilder` instances.
Currently only option is `SkipComboCalculation` which will skip calculating combos when producing a pattern.

### Seqeasy.StringPattern
**Modified**
Fixed double call to CopyToken resulting in bad performance.

## 0.0.1.4

### Seqeasy
**Modified**
Fixed bug in `PatternBuilder<T>.AddSymbol`

### Seqeasy.StringPattern
**Added**
Symbol token converter