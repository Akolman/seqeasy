﻿namespace Seqeasy.Extensions
{
    /// <summary>
    /// Static extension methods for <see cref="GeneratorContext"/>.
    /// </summary>
    public static class ContextAccess
    {
        /// <summary>
        /// Gets the internal <see cref="GeneratorContext"/> of a <see cref="SequenceGenerator"/>.
        /// </summary>
        /// <param name="generator">The SequenceGenerator to retrieve the context for.</param>
        /// <returns>The GeneratorContext for this generator.</returns>
        public static GeneratorContext GetGeneratorContext(SequenceGenerator generator) => generator.Context;
    }
}
