﻿using System;
using System.Text;

using Seqeasy.Extensions.ActionSequence;

namespace Seqeasy.Generator.Section
{
    /// <summary>
    /// Represents a user-defined section with asynchronous logic.
    /// </summary>
    public class AsyncActionSection : IGeneratorSection
    {
        private readonly ISequenceGeneratorAsyncAction action;
        private readonly GeneratorContext generatorContext;

        /// <summary>
        /// Initializes a new instance of the <see cref="AsyncActionSection"/> class.
        /// </summary>
        /// <param name="token">The ActionToken for this section.</param>
        /// <param name="context">The context.</param>
        /// <exception cref="InvalidOperationException">Thrown if action is of wrong type.</exception>
        internal AsyncActionSection(ActionToken token, GeneratorContext context)
        {
            if (Activator.CreateInstance(token.ActionType) is ISequenceGeneratorAsyncAction generatorAction)
            {
                action = generatorAction;
                generatorContext = context;
            }
            else
            {
                throw new InvalidOperationException($"Provided token uses {nameof(ActionToken.ActionType)} of type {token.ActionType.GetType()} but must be {nameof(ISequenceGeneratorAsyncAction)}");
            }
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        public int Id { get; set; } = 0;

        /// <inheritdoc/>
        void IGeneratorSection.AppendNextSection(StringBuilder sb)
        {
            action.HandleAsync(generatorContext, sb).GetAwaiter().GetResult();
        }
    }
}
