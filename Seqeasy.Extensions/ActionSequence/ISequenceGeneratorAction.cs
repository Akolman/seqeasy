﻿using System;
using System.Text;

namespace Seqeasy.Extensions.ActionSequence
{
    /// <summary>
    /// Interface that represents a synchronous user-defined action that is executed during generation.
    /// </summary>
    public interface ISequenceGeneratorAction
    {
        /// <summary>
        /// User-defined function that is executed every generation.
        /// </summary>
        /// <param name="generatorContext">The current generator context.</param>
        /// <param name="builder">The builder defining the string currently being produced.</param>
        public void Handle(GeneratorContext generatorContext, StringBuilder builder);
    }
}
