﻿using System;
using Seqeasy.Design;
using Seqeasy.Generator.Section;

namespace Seqeasy.Extensions.ActionSequence
{
    /// <summary>
    /// Base class for user-defined token types..
    /// </summary>
    public abstract record ActionToken : SectionToken
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ActionToken"/> class.
        /// </summary>
        /// <param name="actionType">The ActionToken implementation type.</param>
        protected ActionToken(Type actionType)
        {
            ActionType = actionType;
        }

        private ActionToken()
        {
            throw new InvalidOperationException($"Don't call empty constructor of {nameof(ActionToken)}");
        }

        /// <summary>
        /// Gets the Type associated with this ActionToken.
        /// Should be <see cref="ISequenceGeneratorAction"/>
        /// or <see cref="ISequenceGeneratorAsyncAction"/> implementation.
        /// </summary>
        public Type ActionType { get; }
    }

    /// <summary>
    /// Generic class for user-defined token types.
    /// </summary>
    /// <typeparam name="T">The implementation type.</typeparam>
    public record ActionToken<T> : ActionToken
        where T : ISequenceGeneratorAction
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ActionToken&lt;T&gt;"/> class.
        /// </summary>
        internal ActionToken()
            : base(typeof(T))
        {
        }

        /// <inheritdoc/>
        protected override void AddSection(GeneratorContext generatorContext)
        {
            generatorContext.Generators.Add(new ActionSection(this, generatorContext));
        }
    }

    /// <summary>
    /// Generic class for async user-defined token types.
    /// </summary>
    /// <typeparam name="T">The implementation type.</typeparam>
    public record AsyncActionToken<T> : ActionToken
        where T : ISequenceGeneratorAsyncAction
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AsyncActionToken&lt;T&gt;"/> class.
        /// </summary>
        public AsyncActionToken()
            : base(typeof(T))
        {
        }

        /// <inheritdoc/>
        protected override void AddSection(GeneratorContext generatorContext)
        {
            generatorContext.Generators.Add(new AsyncActionSection(this, generatorContext));
        }
    }
}
