﻿using System;
using System.Text;
using System.Threading.Tasks;

namespace Seqeasy.Extensions.ActionSequence
{
    /// <summary>
    /// Interface that represents a asynchronous user-defined action that is executed during generation.
    /// </summary>
    public interface ISequenceGeneratorAsyncAction
    {
        /// <summary>
        /// User-defined function that is executed every generation.
        /// </summary>
        /// <param name="generatorContext">The current generator context.</param>
        /// <param name="builder">The builder defining the string currently being produced.</param>
        /// <returns>An awaitable Task.</returns>
        public Task HandleAsync(GeneratorContext generatorContext, StringBuilder builder);
    }
}
