﻿using System;

namespace Seqeasy.Extensions.ActionSequence
{
    /// <summary>
    /// <see cref="ActionToken&lt;T&gt;"/> and <see cref="AsyncActionToken&lt;T&gt;"/>
    /// <see cref="PatternBuilder"/> extensions.
    /// </summary>
    public static class BuilderExtensions
    {
        /// <summary>
        /// Adds a section of an implementation of a <see cref="ISequenceGeneratorAction"/> type T to the current builder.
        /// </summary>
        /// <typeparam name="T">The ISequenceGeneratorAction implementation type.</typeparam>
        /// <param name="builder">This builder.</param>
        /// <returns>The current builder.</returns>
        public static PatternBuilder AddAction<T>(this PatternBuilder builder)
            where T : ISequenceGeneratorAction
        {
            return builder.AddToken(new ActionToken<T>());
        }

        /// <summary>
        /// Adds a section of an implementation  of a <see cref="ISequenceGeneratorAsyncAction"/> type T to the current builder.
        /// </summary>
        /// <typeparam name="T">The ISequenceGeneratorAsyncAction impelmentation type.</typeparam>
        /// <param name="builder">This builder.</param>
        /// <returns>The current builder.</returns>
        public static PatternBuilder AddAsyncAction<T>(this PatternBuilder builder)
            where T : ISequenceGeneratorAsyncAction
        {
            return builder.AddToken(new AsyncActionToken<T>());
        }
    }
}
